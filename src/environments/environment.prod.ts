/**
 * Kazam Admin
 * @name Prod Environment
 * @file src/environments/environment.stage.ts
 * @author Joel Cano
 */
export const environment = {
  production: true,
  api: {
    protocol: 'https',
    host: 'api.kazamapp.mx',
    port: 443,
    namespace: '/admin'
  },
  redirectDelay: 1000,
  storage: 'auth_app_token',
  createdAt: 'merchant_created_at',
  image: {
    extensions: '.jpeg,.png,.jpg',
    limit: 1000000 // 1MB
  },
  maps: {
    // CDMX 19.4310609,-99.1340312
    center: {
      lat: 19.4310609,
      lng: -99.1340312
    },
    zoom: 12
  },
  // TODO: remove
  google: {
    maps: 'AIzaSyCUMQQh5sxKgH0jlj5RWKmJx_vZa82WLV8'
  }
};
