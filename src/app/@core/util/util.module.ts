/**
 * Kazam Admin
 * @name Core Util Module
 * @file src/app/@core/util/util.module.ts
 * @author Joel Cano
 */
// Angular imports
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
// Application imports
import { AuthGuard } from './auth.guard';
// Application data services
const services = [
  AuthGuard
];

@NgModule({
  imports: [CommonModule],
  providers: [...services]
})
export class UtilModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: UtilModule,
      providers: [...services]
    };
  }
}
