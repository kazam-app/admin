/**
 * Kazam Admin
 * @name Core Data Module
 * @file src/app/@core/data/data.module.ts
 * @author Joel Cano
 */
// Angular imports
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
// Application imports
import { UserService } from './user.service';
import { ShopService } from './shop.service';
import { ImageService } from './image.service';
import { CategoryService } from './category.service';
import { MerchantService } from './merchant.service';
import { ShopClusterService } from './shop-cluster.service';
import { OrganizationService } from './organization.service';
// Application data services
const services = [
  UserService,
  ShopService,
  ImageService,
  CategoryService,
  MerchantService,
  ShopClusterService,
  OrganizationService
];

@NgModule({
  imports: [CommonModule],
  providers: [...services]
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [...services]
    };
  }
}
