/**
 * Kazam Admin
 * @name ShopCluster Service
 * @file src/app/@core/data/services/shop-cluster.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { DataService } from './data.service';
import { ShopCluster } from '../models/shop-cluster.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class ShopClusterService extends DataService {
  /**
   * Constructs an ShopClusterService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/shop_clusters');
  }

  /**
   * Get ShopClusters.
   * @return {Promise<ShopCluster[]>}  ShopCluster array promise
   */
  list(): Promise<ShopCluster[]> {
    return this.get<ShopCluster[]>();
  }

  /**
   * Get a single ShopCluster.
   * @param {number} id               ShopCluster id
   * @return {Promise<ShopCluster>}   ShopCluster promise
   */
  show(id: number): Promise<ShopCluster> {
    return this.get<ShopCluster>(`/${id}`);
  }

  /**
   * Create a ShopCluster.
   * @param {ShopCluster} shopCluster   ShopCluster data
   * @return {Promise<ShopCluster>}     ShopCluster instance promise
   */
  create(shopCluster: ShopCluster): Promise<ShopCluster> {
    return this.post<ShopCluster>({ shop_cluster: shopCluster });
  }

  /**
   * Update a ShopCluster by id.
   * @param {ShopCluster} shopCluster   ShopCluster data
   * @return {Promise<ShopCluster>}     ShopCluster instance promise
   */
  update(shopCluster: ShopCluster): Promise<ShopCluster> {
    return this.put<ShopCluster>(`/${shopCluster.id}`, {
      shop_cluster: shopCluster
    });
  }

  /**
   * Remove a ShopCluster.
   * @return {Promise<ShopCluster>}  ShopCluster instance promise
   */
  destroy(id: number): Promise<ShopCluster> {
    return this.delete<ShopCluster>(`/${id}`);
  }
}
