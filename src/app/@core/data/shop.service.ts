/**
 * Kazam Admin
 * @name Shop Service
 * @file src/app/@core/data/services/shop.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from './data.service';
import { Shop } from '../models/shop.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class ShopService extends DataService {
  /**
   * Cosntructs an ShopService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/merchants');
  }
  /**
   * Get Shops for a Merchant.
   * @return {Promise<Shop[]>}  Shop array promise
   */
  list(id: number): Promise<Shop[]> {
    return this.get<Shop[]>(`/${id}/shops`);
  }
  /**
   * Get a single Shop.
   * @param {number} id            Shop id
   * @return {Promise<Shop>}   Shop promise
   */
  show(merchantId: number, id: number): Promise<Shop> {
    return this.get<Shop>(`/${merchantId}/shops/${id}`);
  }
  /**
   * Create a Shop.
   * @param {Shop} shop    Shop data
   * @return {Promise<Shop>}   Shop instance promise
   */
  create(merchantId: number, shop: Shop): Promise<Shop> {
    return this.post<Shop>({ shop: shop }, `/${merchantId}/shops`);
  }
  /**
   * Update a Shop by id.
   * @param {Shop} shop    Shop data
   * @return {Promise<Shop>}   Shop instance promise
   */
  update(merchantId: number, shop: Shop): Promise<Shop> {
    return this.put<Shop>(`/${merchantId}/shops/${shop.id}`, { shop: shop });
  }
  /**
   * Remove a Shop.
   * @return {Promise<Shop>}  Shop instance promise
   */
  destroy(merchantId: number, id: number): Promise<Shop> {
    return this.delete<Shop>(`/${merchantId}/shops/${id}`);
  }
}
