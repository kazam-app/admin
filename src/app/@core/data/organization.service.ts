/**
 * Kazam Admin
 * @name Organization Service
 * @file src/app/@core/data/organization.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from './data.service';
import { Organization } from '../models/organization.model';

@Injectable()
export class OrganizationService extends DataService {
  /**
   * Constructs an OrganizationService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/organizations');
  }
  /**
   * Get all Organizations.
   * @return {Promise<Organization[]>} Organizations array promise
   */
  list(): Promise<Organization[]> {
    return this.get<Organization[]>();
  }
  /**
   * Get a single Organization.
   * @param {number} id            Organization id
   * @return {Promise<Organization>}   Organization promise
   */
  show(id: number): Promise<Organization> {
    return this.get<Organization>(`/${id}`);
  }
  /**
   * Create a Organization.
   * @param {Organization} organization    Organization data
   * @return {Promise<Organization>}   Organization instance promise
   */
  create(organization: Organization): Promise<Organization> {
    return this.post<Organization>({ organization: organization });
  }
  /**
   * Update a Organization by id.
   * @param {Organization} organization    Organization data
   * @return {Promise<Organization>}   Organization instance promise
   */
  update(organization: Organization): Promise<Organization> {
    return this.put<Organization>(`/${organization.id}`, { organization: organization });
  }
  /**
   * Remove a Organization.
   * @return {Promise<Organization>}  Organization instance promise
   */
  destroy(id: number): Promise<Organization> {
    return this.delete<Organization>(`/${id}`);
  }
}
