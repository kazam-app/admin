/**
 * Kazam Admin
 * @name Category Service
 * @file src/app/@core/data/services/category.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from './data.service';
import { Category } from '../models/category.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class CategoryService extends DataService {
  /**
   * Cosntructs an CategoryService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/categories');
  }
  /**
   * Get Categories.
   * @return {Promise<Category[]>}  Category array promise
   */
  list(): Promise<Category[]> {
    return this.get<Category[]>();
  }
  /**
   * Get a single Category.
   * @param {number} id            Category id
   * @return {Promise<Category>}   Category promise
   */
  show(id: number): Promise<Category> {
    return this.get<Category>(`/${id}`);
  }
  /**
   * Create a Category.
   * @param {Category} category    Category data
   * @return {Promise<Category>}   Category instance promise
   */
  create(category: Category): Promise<Category> {
    return this.post<Category>({ category: category });
  }
  /**
   * Update a Category by id.
   * @param {Category} category    Category data
   * @return {Promise<Category>}   Category instance promise
   */
  update(category: Category): Promise<Category> {
    return this.put<Category>(`/${category.id}`, { category: category });
  }
  /**
   * Remove a Category.
   * @return {Promise<Category>}  Category instance promise
   */
  destroy(id: number): Promise<Category> {
    return this.delete<Category>(`/${id}`);
  }
}
