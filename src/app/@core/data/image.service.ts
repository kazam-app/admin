/**
 * Kazam Admin
 * @name Image Service
 * @file src/app/@core/data/services/image.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
// Application
import { environment } from '../../../environments/environment';

@Injectable()
export class ImageService {
  limit = environment.image.limit;
  extensions = environment.image.extensions;
  // https://nehalist.io/uploading-files-in-angular2/
  fileChange(event): FileReader {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
    }
    return reader;
  }
}
