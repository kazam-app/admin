/**
 * Kazam Admin
 * @name Merchant Service
 * @file src/app/@core/data/services/merchant.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import * as moment from 'moment';
import { DataService } from './data.service';
import { Merchant } from '../models/merchant.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class MerchantService extends DataService {
  /**
   * Constructs an MerchantService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/merchants');
  }
  /**
   * Get Merchants.
   * @return {Promise<Merchant[]>}  Merchant array promise
   */
  list(): Promise<Merchant[]> {
    return this.get<Merchant[]>();
  }
  /**
   * Set created at in local storage
   */
  setCreatedAt(merchant: Merchant): void {
    localStorage.setItem(environment.createdAt, moment(merchant.created_at).toISOString());
  }
  /**
   * Get created_at stored in local storage
   * @return {string}   Merchant created_at
   */
  getCreatedAt(): string {
    return localStorage.getItem(environment.createdAt);
  }
  /**
   * Get a single Merchant.
   * @param {number} id            Merchant id
   * @return {Promise<Merchant>}   Merchant promise
   */
  show(id: number): Promise<Merchant> {
    return this.get<Merchant>(`/${id}`);
  }
  /**
   * Create a Merchant.
   * @param {Merchant} merchant    Merchant data
   * @return {Promise<Merchant>}   Merchant instance promise
   */
  create(merchant: Merchant): Promise<Merchant> {
    return this.post<Merchant>({ merchant: merchant });
  }
  /**
   * Update a Merchant by id.
   * @param {Merchant} merchant    Merchant data
   * @return {Promise<Merchant>}   Merchant instance promise
   */
  update(merchant: Merchant): Promise<Merchant> {
    return this.put<Merchant>(`/${merchant.id}`, { merchant: merchant });
  }
  /**
   * Remove a Merchant.
   * @return {Promise<Merchant>}  Merchant instance promise
   */
  destroy(id: number): Promise<Merchant> {
    return this.delete<Merchant>(`/${id}`);
  }
}
