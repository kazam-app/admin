/**
 * Kazam Admin
 * @name Organization model
 * @file src/app/@core/models/organization.model.ts
 * @author Joel Cano
 */
export class Organization {
  id: number;
  name: string;
}
