/**
 * Kazam Admin
 * @name ShopCluster model
 * @file src/app/@core/models/shop-cluster.model.ts
 * @author Joel Cano
 */
export class ShopCluster {
  id: number;
  name: string;
  location: string;
  city: string;
  postal_code: string;
  state: string;
  country: string;
}
