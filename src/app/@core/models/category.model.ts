/**
 * Kazam Admin
 * @name Category model
 * @file src/app/@core/models/category.model.ts
 * @author Joel Cano
 */
export class Category {
  id: number;
  name: string;
  image: string;
  image_url: string;
}
