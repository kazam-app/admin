/**
 * Kazam Admin
 * @name Merchant model
 * @file src/app/@core/models/merchant.model.ts
 * @author Joel Cano
 */
export const maxRating = 5;

export class Merchant {
  id: number;
  name: string;
  category?: string;
  organization?: string;
  is_active = true;
  image: string;
  image_url: string;
  budget_rating: number;
  invite_code: string;
  category_id: number;
  organization_id: number;
  background_image: string;
  background_image_url: string;
  created_at: Date;
}
