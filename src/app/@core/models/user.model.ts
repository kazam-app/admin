/**
 * Kazam Admin
 * @name User model
 * @file src/app/@core/models/user.ts
 * @author Joel Cano
 */
export class User {
  id: number;
  name: string;
  last_names: string;
  email: string;
  password?: string;
  password_confirmation?: string;
}
