/**
 * Kazam Admin
 * @name Shop model
 * @file src/app/@core/models/shop.model.ts
 * @author Joel Cano
 */
export class Shop {
  id: number;
  name: string;
  location: string;
  is_active: boolean;
  shop_cluster?: string;
  lat: number;
  lng: number;
}
