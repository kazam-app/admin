/**
 * Kazam Admin
 * @name Core Module
 * @file src/app/@core/core.module.ts
 * @author Joel Cano
 */
// Angular imports
import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf
} from '@angular/core';
// Framework imports
import { NbEmailPassAuthProvider, NbAuthModule } from '@nebular/auth';
import { NB_AUTH_TOKEN_WRAPPER_TOKEN, NbAuthJWTToken } from '@nebular/auth';
// Application imports
import { DataModule } from './data/data.module';
import { UtilModule } from './util/util.module';
import { throwIfAlreadyLoaded } from './module-import-guard';
// Config
import { environment } from '../../environments/environment';
const api = environment.api;
const endpoint = api.protocol + '://' + api.host + ':' + api.port;

const coreProviders = [
  // Application DataModule
  ...DataModule.forRoot().providers,
  // Nebular AuthModule
  ...NbAuthModule.forRoot({
    providers: {
      email: {
        service: NbEmailPassAuthProvider,
        config: {
          baseEndpoint: endpoint,
          // login config
          login: {
            endpoint: api.namespace + '/login',
            redirect: { success: '/merchants' },
            defaultErrors: ['Los datos son incorrectos, intenta otra vez.'],
            defaultMessages: ['Iniciando sesión']
          },
          logout: {
            endpoint: api.namespace + '/log_out',
            redirect: { success: '/auth/login', failure: '/auth/login' }
          },
          token: { key: 'token' },
          errors: { key: 'errors' }
        }
      }
    },
    forms: {
      login: { redirectDelay: environment.redirectDelay },
      logout: { redirectDelay: environment.redirectDelay },
      validation: {
        email: { required: true },
        password: { required: true, minLength: 8 }
      }
    }
  }).providers,
  { provide: NB_AUTH_TOKEN_WRAPPER_TOKEN, useClass: NbAuthJWTToken },
  // Extra Providers below
  ...UtilModule.forRoot().providers
];

@NgModule({
  imports: [CommonModule],
  exports: [NbAuthModule],
  declarations: [],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [...coreProviders]
    };
  }
}
