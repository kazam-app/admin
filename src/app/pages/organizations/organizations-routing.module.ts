/**
 * Kazam Admin
 * @name Organizations Routing Module
 * @file src/app/pages/organizations/organizations-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { OrganizationsComponent } from './organizations.component';
import { OrganizationComponent } from './organization/organization.component';
import { OrganizationListComponent } from './organization-list/organization-list.component';

const routes: Routes = [{
  path: '',
  component: OrganizationsComponent,
  children: [{
    path: 'list',
    component: OrganizationListComponent
  }, {
    path: 'new',
    component: OrganizationComponent
  }, {
    path: ':id',
    component: OrganizationComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrganizationsRoutingModule {}

export const routedComponents = [
  OrganizationsComponent,
  OrganizationComponent,
  OrganizationListComponent
];
