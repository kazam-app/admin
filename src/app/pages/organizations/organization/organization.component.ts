/**
 * Kazam Admin
 * @name Organization Component
 * @file src/app/pages/organizations/organization/organization.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { FormComponent } from '../../shared/form/form.component';
import { Organization } from '../../../@core/models/organization.model';
import { OrganizationService } from '../../../@core/data/organization.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html'
})
export class OrganizationComponent extends FormComponent<Organization> implements OnInit {
  /**
   * Constructs an OrganizationComponent instance.
   * @param {Router} router                             Router instance
   * @param {ActivatedRoute} route                      ActivatedRoute instance
   * @param {OrganizationService} organizationService   OrganizationService instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private organizationService: OrganizationService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.organizationService.update(this.element);
    } else {
      submision = this.organizationService.create(this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * Set category via service.
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.organizationService.show(id)
      .then(element => this.element = element)
      .then(() => this.loaded = true);
  }
  /**
   * New element
   *
   * @protected
   */
  protected newElement(): void {
    this.element = new Organization();
  }
  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate(['pages/organizations/list']);
  }
}
