/**
 * Kazam Admin
 * @name Organizations Component
 * @file src/app/pages/organizations/organizations.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-organizations',
  template: `<router-outlet></router-outlet>`
})
export class OrganizationsComponent {}
