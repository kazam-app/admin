/**
 * Kazam Admin
 * @name Organizations Module
 * @file src/app/pages/organizations/organizations.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { OrganizationsRoutingModule, routedComponents } from './organizations-routing.module';

@NgModule({
  imports: [
    // Angular
    ThemeModule,
    // Component
    OrganizationsRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents],
  entryComponents: [KzmConfirmComponent]
})
export class OrganizationsModule {}
