/**
 * Kazam Admin
 * @name OrganizationList Component
 * @file src/app/pages/organization-list/organization-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { OrganizationService } from '../../../@core/data/organization.service';

@Component({
  selector: 'app-organization-list',
  templateUrl: './organization-list.component.html'
})
export class OrganizationListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    columns: {
      name: { title: 'Name', filter: false }
    }
  });

  /**
   * Constructs an OrganizationListComponent instance.
   * @param {Router} router             Router instance
   * @param {NgbModal} modalService     NgbModal instance
   * @param {OrganizationService} organizationService   OrganizationService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private organizationService: OrganizationService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.load();
  }

  /**
   * onCreate Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate(['pages/organizations/new']);
  }

  /**
   * onEdit Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/organizations/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que la quieres borrar?';
    activeModal.result.then((result) => {
      if (result) {
        this.organizationService.destroy(event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Retrive a data array from the server and load it as the table source.
   *
   * @private
   */
  private load(): void {
    this.organizationService.list().then(organizations => this.source.load(organizations));
  }
}
