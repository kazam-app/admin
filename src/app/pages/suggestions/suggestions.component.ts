/**
 * Kazam Admin
 * @name Suggestions Component
 * @file src/app/pages/suggestions/suggestions.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-suggestions',
  template: `<router-outlet></router-outlet>`
})
export class SuggestionsComponent {}
