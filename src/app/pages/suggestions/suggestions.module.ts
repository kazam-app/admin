/**
 * Kazam Admin
 * @name Suggestions Module
 * @file src/app/pages/suggestions/suggestions.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { SuggestionsRoutingModule, routedComponents } from './suggestions-routing.module';

@NgModule({
  imports: [
    // Angular
    ThemeModule,
    // Component
    SuggestionsRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents],
  entryComponents: [KzmConfirmComponent]
})
export class SuggestionsModule {}
