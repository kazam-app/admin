/**
 * Kazam Admin
 * @name Suggestion model
 * @file src/app/pages/suggestions/suggestion.model.ts
 * @author Joel Cano
 */
export class Suggestion {
  id: number;
  name: string;
  created_at: string;
}
