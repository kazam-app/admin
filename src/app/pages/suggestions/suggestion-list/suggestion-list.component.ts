/**
 * Kazam Admin
 * @name SuggestionListComponent Component
 * @file src/app/pages/suggestion-list/suggestion-list.component.ts
 * @author Joel Cano
 */
// Angular
import { DatePipe } from '@angular/common';
import { LocalTimePipe } from 'angular2-moment';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { SuggestionService } from './suggestion.service';

@Component({
  selector: 'app-suggestion-list',
  templateUrl: './suggestion-list.component.html',
  providers: [DatePipe, LocalTimePipe, SuggestionService]
})
export class SuggestionListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    mode: 'external',
    actions: false,
    columns: {
      name: { title: 'Sugerencia', filter: false },
      created_at: {
        title: 'Fecha',
        filter: false,
        valuePrepareFunction: (value) => {
          const local = this.localTimePipe.transform(value);
          return this.datePipe.transform(local, 'medium');
        }
      },
      'user.identifier': {
        title: 'Usuario',
        filter: false,
        valuePrepareFunction: (value, suggestion) => {
          return suggestion.user.identifier;
        }
      },
      'user.name': {
        title: 'Nombre',
        filter: false,
        valuePrepareFunction: (value, suggestion) => {
          return suggestion.user.name;
        }
      },
      'user.email': {
        title: 'Correo',
        filter: false,
        valuePrepareFunction: (value, suggestion) => {
          return suggestion.user.email;
        }
      }
    }
  });

  /**
   * Constructs an SuggestionListComponent instance.
   * @param {DatePipe} datePipe                      DatePipe injected instance
   * @param {LocalTimePipe} localTimePipe            LocalTimePipe injected instance
   * @param {SuggestionService} suggestionService    SuggestionService injected instance
   *
   * @constructor
   */
  constructor(
    private datePipe: DatePipe,
    private localTimePipe: LocalTimePipe,
    private suggestionService: SuggestionService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.suggestionService.list().then(suggestions => this.source.load(suggestions));
  }
}
