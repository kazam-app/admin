/**
 * Kazam Admin
 * @name Suggestion Service
 * @file src/app/pages/suggestions/suggestion-list/suggestion.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { Suggestion } from './suggestion.model';
import { DataService } from '../../../@core/data/data.service';

@Injectable()
export class SuggestionService extends DataService {
  /**
   * Constructs an SuggestionService instance.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/suggestions');
  }
  /**
   * Get Suggestions from
   * @return {Promise<Suggestion[]>} Suggestion data
   */
  list(): Promise<Suggestion[]> {
    return this.get<Suggestion[]>();
  }
}
