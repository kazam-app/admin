/**
 * Kazam Admin
 * @name Suggestions Routing Module
 * @file src/app/pages/suggestions/suggestions-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { SuggestionsComponent } from './suggestions.component';
import { SuggestionListComponent } from './suggestion-list/suggestion-list.component';

const routes: Routes = [{
  path: '',
  component: SuggestionsComponent,
  children: [{
    path: 'list',
    component: SuggestionListComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuggestionsRoutingModule {}

export const routedComponents = [
  SuggestionsComponent,
  SuggestionListComponent
];
