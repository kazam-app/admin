/**
 * Kazam Admin
 * @name Dashboard Module
 * @file src/app/pages/dashboard/dashboard.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
// Application
import { DashboardComponent } from './dashboard.component';

const components = [
  DashboardComponent
];

@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule
  ],
  declarations: [...components],
})
export class DashboardModule {}
