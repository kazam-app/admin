/**
 * Kazam Admin
 * @name Dashboard Component
 * @file src/app/pages/dashboard/dashboard.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
// Theme
// import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {}

}
