/**
 * Kazam Admin
 * @name ShopClusterList Component
 * @file src/app/pages/shop-cluster-list/shop-cluster-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import {
  KzmConfirmComponent
} from '../../../@theme/components/modal/confirm.component';
// Application
import { ShopClusterService } from '../../../@core/data/shop-cluster.service';

@Component({
  selector: 'app-shop-cluster-list',
  templateUrl: './shop-cluster-list.component.html'
})
export class ShopClusterListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    columns: {
      name: { title: 'Nombre', filter: false },
      location: { title: 'Dirección', filter: false }
    }
  });

  /**
   * Constructs an ShopClusterListComponent instance.
   * @param {Router} router                          Router instance
   * @param {NgbModal} modalService                  NgbModal instance
   * @param {ShopClusterService} shopClusterService  ShopClusterService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private shopClusterService: ShopClusterService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.load();
  }

  /**
   * onCreate Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate(['pages/shop-clusters/new']);
  }

  /**
   * onEdit Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/shop-clusters/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, {
      size: 'lg',
      container: 'nb-layout'
    });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que lo quieres borrar?';
    activeModal.result.then((result) => {
      if (result) {
        this.shopClusterService.destroy(event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Get shop clusters via the service.
   *
   * @private
   */
  private load(): void {
    this.shopClusterService.list()
      .then(shopClusters => this.source.load(shopClusters));
  }
}
