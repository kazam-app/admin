/**
 * Kazam Admin
 * @name ShopCluster Component
 * @file src/app/pages/shop-clusters/shop-cluster/shop-cluster.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { FormComponent } from '../../shared/form/form.component';
import { ShopCluster } from '../../../@core/models/shop-cluster.model';
import { ShopClusterService } from '../../../@core/data/shop-cluster.service';

@Component({
  selector: 'app-shop-cluster',
  templateUrl: './shop-cluster.component.html'
})
export class ShopClusterComponent extends FormComponent<ShopCluster> implements OnInit {
  /**
   * Constructs an ShopClusterComponent instance.
   * @param {Router} router                           Router instance
   * @param {ActivatedRoute} route                    ActivatedRoute instance
   * @param {ShopClusterService} shopClusterService   ShopClusterService
   *  instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private shopClusterService: ShopClusterService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.shopClusterService.update(this.element);
    } else {
      submision = this.shopClusterService.create(this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * Set category via service.
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.shopClusterService.show(id)
      .then(element => this.element = element)
      .then(() => this.loaded = true);
  }

  /**
   * New element
   *
   * @protected
   */
  protected newElement(): void {
    this.element = new ShopCluster();
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate(['pages/shop-clusters/list']);
  }
}
