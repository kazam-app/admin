/**
 * Kazam Admin
 * @name ShopClusters Component
 * @file src/app/pages/shop-clusters/shop-clusters.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-shop-clusters',
  template: `<router-outlet></router-outlet>`
})
export class ShopClustersComponent {}
