/**
 * Kazam Admin
 * @name ShopClusters Module
 * @file src/app/pages/shop-clusters/shop-clusters.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
// Application
import { ShopClustersRoutingModule, routedComponents } from './shop-clusters-routing.module';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    ThemeModule,
    // Component
    ShopClustersRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents]
})
export class ShopClustersModule {}
