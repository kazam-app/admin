/**
 * Kazam Admin
 * @name ShopClusters Routing Module
 * @file src/app/pages/shop-clusters/shop-clusters-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { ShopClustersComponent } from './shop-clusters.component';
import { ShopClusterComponent } from './shop-cluster/shop-cluster.component';
import {
  ShopClusterListComponent
} from './shop-cluster-list/shop-cluster-list.component';

const routes: Routes = [{
  path: '',
  component: ShopClustersComponent,
  children: [{
    path: 'list',
    component: ShopClusterListComponent
  }, {
    path: 'new',
    component: ShopClusterComponent
  }, {
    path: ':id',
    component: ShopClusterComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopClustersRoutingModule {}

export const routedComponents = [
  ShopClustersComponent,
  ShopClusterComponent,
  ShopClusterListComponent
];
