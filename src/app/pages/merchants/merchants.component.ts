/**
 * Kazam Admin
 * @name Merchants Component
 * @file src/app/pages/merchants/merchants.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-merchants',
  template: `<router-outlet></router-outlet>`
})
export class MerchantsComponent {}
