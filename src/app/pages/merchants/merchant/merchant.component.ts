/**
 * Kazam Admin
 * @name Merchant Component
 * @file src/app/pages/merchants/merchant/merchant.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { Category } from '../../../@core/models/category.model';
import { FormComponent } from '../../shared/form/form.component';
import { ImageService } from '../../../@core/data/image.service';
import { MerchantService } from '../../../@core/data/merchant.service';
import { CategoryService } from '../../../@core/data/category.service';
import { Organization } from '../../../@core/models/organization.model';
import { maxRating, Merchant } from '../../../@core/models/merchant.model';
import { OrganizationService } from '../../../@core/data/organization.service';

@Component({
  selector: 'app-merchant',
  templateUrl: './merchant.component.html',
  styleUrls: ['./merchant.component.scss']
})
export class MerchantComponent extends FormComponent<Merchant> implements OnInit {
  ratings: any;
  imgLoaded = false;
  extensions: string;
  bgImgLoaded = false;
  categories: Category[];
  organizations: Organization[];

  /**
   * Constructs an MerchantComponent instance.
   * @param {Router} router                             Router injected instance
   * @param {ActivatedRoute} route                      ActivatedRoute injected instance
   * @param {MerchantService} merchantService           MerchantService injected instance
   * @param {CategoryService} categoryService           CategoryService injected instance
   * @param {OrganizationService} organizationService   OrganizationService injected instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private imageService: ImageService,
    private merchantService: MerchantService,
    private categoryService: CategoryService,
    private organizationService: OrganizationService) {
      super(router, route);
      this.extensions = this.imageService.extensions;
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
    this.setForm();
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.merchantService.update(this.element);
    } else {
      submision = this.merchantService.create(this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * File on change event
   * @param {any} event File event instance
   */
  logoChange(event) {
    const reader = this.imageService.fileChange(event);
    reader.onload = () => {
      this.element.image = reader.result;
      this.imgLoaded = true;
    };
  }

  /**
   * File on change event
   * @param {any} event File event instance
   */
  backgroundChange(event) {
    const reader = this.imageService.fileChange(event);
    reader.onload = () => {
      this.element.background_image = reader.result;
      this.bgImgLoaded = true;
    };
  }

  /**
   * Get Merchant via id.
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.merchantService.show(id)
      .then(element => this.element = element)
      .then(() => {
        this.loaded = true;
        this.imgLoaded = true;
        this.bgImgLoaded = true;
      });
  }

  /**
   * New element
   *
   * @protected
   */
  protected newElement(): void {
    this.element = new Merchant();
  }

  /**
   * Prepare the data for the form.
   *
   * @private
   */
  private setForm(): void {
    this.ratings = Array(maxRating).fill(0).map((x, i) => i);
    this.organizationService.list()
      .then(organizations => this.organizations = organizations);
    this.categoryService.list()
    .then(categories => this.categories = categories);
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate(['pages/merchants/list']);
  }
}
