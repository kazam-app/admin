/**
 * Kazam Admin
 * @name User Service
 * @file src/app/pages/merchants/shared/user.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { User } from './user.model';
import { DataService } from '../../../@core/data/data.service';

@Injectable()
export class UserService extends DataService {
  /**
   * Constructs an UserService instance.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/merchants');
  }
  /**
   * Get Users for a Merchant.
   * @param {number} merchantId   Merchant id
   * @return {Promise<User[]>}    Users array promise
   */
  list(merchantId: number): Promise<User[]> {
    return this.get<User[]>(`/${merchantId}/users`);
  }
  /**
   * Get a single User.
   * @param {number} id        User id
   * @return {Promise<User>}   User promise
   */
  show(merchantId: number, id: number): Promise<User> {
    return this.get<User>(`/${merchantId}/users/${id}`);
  }
  /**
   * Create a User.
   * @param {User} shop        User data
   * @return {Promise<User>}   User instance promise
   */
  create(merchantId: number, user: User): Promise<User> {
    return this.post<User>({ user: user }, `/${merchantId}/users`);
  }
  /**
   * Update a User by id.
   * @param {User} user        User data
   * @return {Promise<User>}   User instance promise
   */
  update(merchantId: number, user: User): Promise<User> {
    return this.put<User>(`/${merchantId}/shops/${user.id}`, { user: user });
  }
  /**
   * Remove a User.
   * @return {Promise<User>}  User instance promise
   */
  destroy(merchantId: number, id: number): Promise<User> {
    return this.delete<User>(`/${merchantId}/users/${id}`);
  }
}
