/**
 * Kazam Admin
 * @name User model
 * @file src/app/pages/merchants/shared/user.model.ts
 * @author Joel Cano
 */

export const Roles = ['Lectura', 'Lectura y Escritura'];

export class User {
  id: number;
  role: number;
  merchant_id?: number;
  organization_user_id?: number;
  organization_user?: {
    name: string;
    last_names: string;
    email: string;
  };
}
