/**
 * Kazam Admin
 * @name Merchants Routing Module
 * @file src/app/pages/merchants/merchants-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { ShopComponent } from './shop/shop.component';
import { UserComponent } from './user/user.component';
import { MerchantsComponent } from './merchants.component';
import { MerchantComponent } from './merchant/merchant.component';
import { ShopListComponent } from './shop-list/shop-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { MerchantListComponent } from './merchant-list/merchant-list.component';

const routes: Routes = [{
  path: '',
  component: MerchantsComponent,
  children: [{
    path: 'list',
    component: MerchantListComponent
  }, {
    path: 'new',
    component: MerchantComponent
  }, {
    path: ':id',
    component: MerchantComponent
  }, {
    path: ':merchantId/shops',
    component: ShopListComponent
  }, {
    path: ':merchantId/users',
    component: UserListComponent
  }, {
    path: ':merchantId/shops/new',
    component: ShopComponent
  }, {
    path: ':merchantId/users/new',
    component: UserComponent
  }, {
    path: ':merchantId/shops/:id',
    component: ShopComponent
  }, {
    path: ':merchantId/users/:id',
    component: UserComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantsRoutingModule {}

export const routedComponents = [
  ShopComponent,
  UserComponent,
  MerchantComponent,
  ShopListComponent,
  UserListComponent,
  MerchantsComponent,
  MerchantListComponent
];
