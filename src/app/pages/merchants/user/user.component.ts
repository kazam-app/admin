/**
 * Kazam Admin
 * @name User Component
 * @file src/app/pages/merchants/user/user.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { User, Roles } from '../shared/user.model';
import { UserService } from '../shared/user.service';
import { Merchant } from '../../../@core/models/merchant.model';
import { FormComponent } from '../../shared/form/form.component';
import { MerchantService } from '../../../@core/data/merchant.service';

@Component({
  selector: 'app-merchant-user',
  templateUrl: './user.component.html'
})
export class UserComponent extends FormComponent<User> implements OnInit {
  roles = Roles;
  users: User[];
  merchant: Merchant;
  merchantId: number;

  /**
   * Constructs an UserComponent instance.
   * @param {Router} router                         Router injected instance
   * @param {ActivatedRoute} route                  ActivatedRoute injected instance
   * @param {UserService} userService               UserService injected instance
   * @param {MerchantService} merchantService       MerchantService injected instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private userService: UserService,
    private merchantService: MerchantService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.merchantId = params.merchantId;
      this.setForm(params.id);
    });
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.userService.update(this.merchantId, this.element);
    } else {
      submision = this.userService.create(this.merchantId, this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * Set category via service.
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.action = 'Editar';
    this.userService.show(this.merchantId, id)
      .then(element => this.element = element)
      .then(() => this.loaded = true);
  }

  /**
   * New element
   *
   * @protected
   */
  protected newElement(): void {
    this.action = 'Crear';
    this.element = new User();
  }

  /**
   * Set form elements via an id or new elements.
   * @param {any} id - id or null
   * @private
   */
  private setForm(id: any): void {
    this.merchantService.show(this.merchantId)
      .then(merchant => this.merchant = merchant)
      .then(() => {
        const val = parseInt(id, 10);
        if (isNaN(val)) {
          this.newElement();
          this.loaded = true;
        } else {
          this.getElement(val);
        }
      });
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate([`pages/merchants/${this.merchantId}/users`]);
  }
}
