/**
 * Kazam Admin
 * @name UserList Component
 * @file src/app/pages/merchants/shop-list/user-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RolePipe } from '../../../@theme/pipes/role.pipe';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-merchant-user-list',
  templateUrl: './user-list.component.html',
  providers: [RolePipe]
})
export class UserListComponent implements OnInit {
  merchantId: number;
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    columns: {
      role: {
        title: 'Permiso',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.rolePipe.transform(value);
        }
      },
      'organization_user.name': {
        title: 'Nombre',
        filter: false,
        valuePrepareFunction: (cell, row) => {
          return row.organization_user.name;
        }
      },
      'organization_user.last_names': {
        title: 'Apellidos',
        filter: false,
        valuePrepareFunction: (cell, row) => {
          return row.organization_user.last_names;
        }
      },
      'organization_user.email': {
        title: 'Correo',
        filter: false,
        valuePrepareFunction: (cell, row) => {
          return row.organization_user.email;
        }
      }
    }
  });

  /**
   * Constructs an UserListComponent instance.
   * @param {Router} router                 Router injected instance
   * @param {RolePipe} rolePipe             RolePipe injected instance
   * @param {ActivatedRoute} route          ActivatedRoute injected instance
   * @param {NgbModal} modalService         NgbModal injected instance
   * @param {UserService} userService       UserService injected instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private rolePipe: RolePipe,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private userService: UserService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.route.params.map(p => p.merchantId).subscribe(id => {
      this.merchantId = id;
      this.load();
    });
  }

  /**
   * Navigate to create page.
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate([`pages/merchants/${this.merchantId}/users/new`]);
  }

  /**
   * Navigate to edit page.
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/merchants/${this.merchantId}/users/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que lo quieres borrar?';
    activeModal.result.then((result) => {
      if (result) {
        this.userService.destroy(this.merchantId, event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Retrive a data array from the server and load it as the table source.
   *
   * @private
   */
  private load(): void {
    this.userService.list(this.merchantId).then(users => this.source.load(users));
  }
}
