/**
 * Kazam Admin
 * @name Shop Component
 * @file src/app/pages/merchants/shop/shop.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Libraries
import { MouseEvent } from '@agm/core';
// Application
import { Marker } from './marker.model';
import { Shop } from '../../../@core/models/shop.model';
import { ShopService } from '../../../@core/data/shop.service';
import { FormComponent } from '../../shared/form/form.component';
import { ShopCluster } from '../../../@core/models/shop-cluster.model';
import { ShopClusterService } from '../../../@core/data/shop-cluster.service';
// Config
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-merchant-shop',
  styleUrls: ['./shop.component.scss'],
  templateUrl: './shop.component.html'
})
export class ShopComponent extends FormComponent<Shop> implements OnInit {
  center: any;
  marker: Marker;
  merchantId: number;
  shopClusters: ShopCluster[];
  zoom = environment.maps.zoom;

  /**
   * Constructs an ShopComponent instance.
   * @param {Router} router                           Router injected instance
   * @param {ActivatedRoute} route                    ActivatedRoute injected instance
   * @param {ShopService} shopService                 ShopService injected instance
   * @param {ShopClusterService} shopClusterService   ShopClusterService injected instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private shopService: ShopService,
    private shopClusterService: ShopClusterService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.merchantId = params.merchantId;
      this.setForm(params.id);
    });
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.shopService.update(this.merchantId, this.element);
    } else {
      submision = this.shopService.create(this.merchantId, this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * Map clicked event
   * @param {MouseEvent} event  AGM MouseEvent instance
   */
  mapClicked(event: MouseEvent): void {
    this.setLocation(event.coords.lat, event.coords.lng);
  }

  /**
   * DragEnd event
   * @param {MouseEvent} event  AGM MouseEvent instance
   */
  dragEnd(event: MouseEvent): void {
    this.setLocation(event.coords.lat, event.coords.lng);
  }

  /**
   * Set category via service.
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.action = 'Editar';
    this.shopService.show(this.merchantId, id)
      .then((element) => {
        this.element = element;
        this.center = { lat: this.element.lat, lng: this.element.lng };
        this.setLocation(this.element.lat, this.element.lng);
      })
      .then(() => this.loaded = true);
  }

  /**
   * New element
   *
   * @protected
   */
  protected newElement(): void {
    this.action = 'Crear';
    this.element = new Shop();
    this.center = environment.maps.center;
  }

  /**
   * Set form elements via an id or new elements.
   * @param {any} id - id or null
   * @private
   */
  private setForm(id: any): void {
    this.shopClusterService.list()
      .then(clusters => this.shopClusters = clusters)
      .then(() => {
        const val = parseInt(id, 10);
        if (isNaN(val)) {
          this.newElement();
          this.loaded = true;
        } else {
          this.getElement(val);
        }
      });
  }

  private setLocation(lat: number, lng: number) {
    if (!this.marker) {
      this.marker = new Marker();
    }
    this.marker.lat = lat;
    this.marker.lng = lng;
    this.element.lat = lat;
    this.element.lng = lng;
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate([`pages/merchants/${this.merchantId}/shops`]);
  }
}
