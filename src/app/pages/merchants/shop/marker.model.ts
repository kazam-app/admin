/**
 * Kazam Admin
 * @name Marker model
 * @file src/app/pages/merchants/shop/marker.model.ts
 * @author Joel Cano
 */

export class Marker {
  lat: number;
  lng: number;
}
