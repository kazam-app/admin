/**
 * Kazam Admin
 * @name ShopList Component
 * @file src/app/pages/merchants/shop-list/shop-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { ShopService } from '../../../@core/data/shop.service';

@Component({
  selector: 'app-merchant-shop-list',
  templateUrl: './shop-list.component.html'
})
export class ShopListComponent implements OnInit {
  merchantId: number;
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    columns: {
      name: { title: 'Nombre', filter: false },
      shop_cluster: { title: 'Cluster', filter: false }
    }
  });

  /**
   * Constructs an ShopListComponent instance.
   * @param {Router} router             Router injected instance
   * @param {ActivatedRoute} route      ActivatedRoute injected instance
   * @param {NgbModal} modalService     NgbModal instance
   * @param {ShopService} shopService   ShopService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private shopService: ShopService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.route.params.map(p => p.merchantId).subscribe(id => {
      this.merchantId = id;
      this.load();
    });
  }

  /**
   * Navigate to create page.
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate([`pages/merchants/${this.merchantId}/shops/new`]);
  }

  /**
   * Navigate to edit page.
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/merchants/${this.merchantId}/shops/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que lo quieres borrar?';
    activeModal.result.then((result) => {
      if (result) {
        this.shopService.destroy(this.merchantId, event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Retrive a data array from the server and load it as the table source.
   *
   * @private
   */
  private load(): void {
    this.shopService.list(this.merchantId).then(shops => this.source.load(shops));
  }
}
