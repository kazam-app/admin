/**
 * Kazam Admin
 * @name Merchants Module
 * @file src/app/pages/merchants/merchants.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Libraries
import { AgmCoreModule } from '@agm/core';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { UserService } from './shared/user.service';
import { MerchantsRoutingModule, routedComponents } from './merchants-routing.module';
// Config
import { environment } from '../../../environments/environment';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    AgmCoreModule.forRoot({ apiKey: environment.google.maps }),
    ThemeModule,
    // Component
    Ng2SmartTableModule,
    MerchantsRoutingModule
  ],
  declarations: [
    ...routedComponents
  ],
  providers: [UserService],
  entryComponents: [KzmConfirmComponent]
})
export class MerchantsModule {}
