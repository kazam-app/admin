/**
 * Kazam Web
 * @name Qr Component
 * @file src/app/pages/qrs/qr/qr.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { Qr } from '../shared/qr.model';
import { QrService } from '../shared/qr.service';
import { Shop } from '../../../@core/models/shop.model';
import { ShopService } from '../../../@core/data/shop.service';
import { Merchant } from '../../../@core/models/merchant.model';
import { FormComponent } from '../../shared/form/form.component';
import { MerchantService } from '../../../@core/data/merchant.service';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.scss']
})
export class QrComponent extends FormComponent<Qr> implements OnInit {
  merchants: Merchant[];
  shops: Shop[];

  /**
   * Constructs an QrComponent instance.
   * @param {Router} router                    Router injected instance
   * @param {ActivatedRoute} route             ActivatedRoute injected instance
   * @param {QrService} qrService              QrService injected instance
   * @param {ShopService} shopService          ShopService injected instance
   * @param {MerchantService} merchantService  MerchantService injected instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private qrService: QrService,
    private shopService: ShopService,
    private merchantService: MerchantService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
    this.merchantService.list().then(merchants => this.merchants = merchants);
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.qrService.update(this.element);
    } else {
      submision = this.qrService.create(this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * Load Shops via a Merchant Id
   * @param {any} value Merchant id
   * @return Promise
   */
  loadShops(value: any): Promise<Shop[]> {
    this.shops = [];
    return this.shopService.list(parseInt(value, 10))
      .then(shops => this.shops = shops);
  }

  /**
   * Set qr via service.
   * @param {number} id Validator id
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.qrService.show(id)
      .then(element => this.element = element)
      .then(() => {
        return this.loadShops(this.element.merchant_id);
      })
      .then(() => this.loaded = true);
  }

  /**
   * New element
   *
   * @protected
   */
  protected newElement(): void {
    this.element = new Qr();
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate(['pages/qrs/list']);
  }
}
