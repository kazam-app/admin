/**
 * Kazam Admin
 * @name QRs Component
 * @file src/app/pages/qrs/qrs.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-qrs',
  template: `<router-outlet></router-outlet>`
})
export class QrsComponent {}
