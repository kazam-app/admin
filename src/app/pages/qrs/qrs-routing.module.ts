/**
 * Kazam Admin
 * @name QRs Routing Module
 * @file src/app/pages/qrs/qrs-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { QrsComponent } from './qrs.component';
import { QrComponent } from './qr/qr.component';
import { QrListComponent } from './qr-list/qr-list.component';

const routes: Routes = [{
  path: '',
  component: QrsComponent,
  children: [{
    path: 'list',
    component: QrListComponent
  }, {
    path: 'new',
    component: QrComponent
  }, {
    path: ':id',
    component: QrComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QrsRoutingModule {}

export const routedComponents = [
  QrsComponent,
  QrListComponent,
  QrComponent
];
