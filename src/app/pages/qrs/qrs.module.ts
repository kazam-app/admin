/**
 * Kazam Admin
 * @name QRs Module
 * @file src/app/pages/qrs/qrs.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { QrsRoutingModule, routedComponents } from './qrs-routing.module';
import { QrService } from './shared/qr.service';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    ThemeModule,
    // Component
    QrsRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents],
  providers: [QrService],
  entryComponents: [KzmConfirmComponent]
})
export class QrsModule {}
