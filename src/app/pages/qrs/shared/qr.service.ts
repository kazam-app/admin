/**
 * Kazam Web
 * @name Qr Service
 * @file src/app/pages/qrs/shared/qr.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from '../../../@core/data/data.service';
import { Qr } from './qr.model';

@Injectable()
export class QrService extends DataService {
  /**
   * Constructs an QrService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/qrs');
  }
  /**
   * Get all Qrs for a certain token.
   * @return {Promise<Qr[]>} Qrs array promise
   */
  list(): Promise<Qr[]> {
    return this.get<Qr[]>();
  }
  /**
   * Get a Qr by id.
   * @return {Promise<Qr>} Qr instance promise
   */
  show(id: number): Promise<Qr> {
    return this.get<Qr>(`/${id}`);
  }
  /**
   * Create a Qr.
   * @return {Promise<Qr>} Qr instance promise
   */
  create(qr: Qr): Promise<Qr> {
    return this.post<Qr>({ qr: qr });
  }
  /**
   * Update a Qr by id.
   * @return {Promise<Qr>} Qr instance promise
   */
  update(qr: Qr): Promise<Qr> {
    return this.put<Qr>(`/${qr.id}`, { qr: qr });
  }
  /**
   * Remove a Qr.
   * @return {Promise<Qr>} Qr instance promise
   */
  destroy(id: number): Promise<Qr> {
    return this.delete<Qr>(`/${id}`);
  }
}
