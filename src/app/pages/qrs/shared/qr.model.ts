/**
 * Kazam Admin
 * @name Qr model
 * @file src/app/pages/qrs/shared/qr.model.ts
 * @author Joel Cano
 */
export class Qr {
  id: number;
  is_active: boolean;
  merchant: string;
  merchant_id: number;
  name: string;
  shop: string;
  shop_id: number;
  image_url: string;
}
