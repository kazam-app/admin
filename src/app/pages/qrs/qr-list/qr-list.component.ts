/**
 * Kazam Admin
 * @name QrList Component
 * @file src/app/pages/qrs/qr-list/qr-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivePipe } from '../../../@theme/pipes/active.pipe';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { QrService } from '../shared/qr.service';

@Component({
  selector: 'app-qr-list',
  templateUrl: './qr-list.component.html',
  providers: [ActivePipe]
})
export class QrListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    columns: {
      name: { title: 'Nombre', filter: false },
      merchant: { title: 'Marca', filter: false },
      shop: { title: 'Tienda', filter: false },
      is_active: {
        title: 'Estado',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.activePipe.transform(value, 'Activo', 'Pausado');
        }
      }
    }
  });

  /**
   * Constructs an QrListComponent instance.
   * @param {Router} router             Router instance
   * @param {QrService} qrService       QrService instance
   * @param {NgbModal} modalService     NgbModal instance
   * @param {ActivePipe} activePipe     ActivePipe instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private qrService: QrService,
    private modalService: NgbModal,
    private activePipe: ActivePipe) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.qrService.list().then(qrs => this.source.load(qrs));
  }

  /**
   * onCreate Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate(['pages/qrs/new']);
  }

  /**
   * onEdit Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/qrs/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que lo quieres borrar?';
    activeModal.result.then((result) => {
      if (result) {
        this.qrService.destroy(event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Retrive a data array from the server and load it as the table source.
   *
   * @private
   */
  private load(): void {
    this.qrService.list().then(qrs => this.source.load(qrs));
  }
}
