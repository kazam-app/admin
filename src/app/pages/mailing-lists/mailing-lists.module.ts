/**
 * Kazam Admin
 * @name MailingLists Module
 * @file src/app/pages/mailing-lists/mailing-lists.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import {
  KzmConfirmComponent
} from '../../@theme/components/modal/confirm.component';
// Application
import {
  MailingListRoutingModule,
  routedComponents
} from './mailing-lists-routing.module';

@NgModule({
  imports: [
    // Angular
    ThemeModule,
    // Component
    MailingListRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents],
  entryComponents: [KzmConfirmComponent]
})
export class MailingListsModule {}
