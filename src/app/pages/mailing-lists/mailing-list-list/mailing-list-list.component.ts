/**
 * Kazam Admin
 * @name MailingListListComponent Component
 * @file src/app/pages/mailing-list-list/mailing-list-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  TableSettings,
  Actions
} from '../../../@core/models/ng2-smart-table.model';
import {
  KzmConfirmComponent
} from '../../../@theme/components/modal/confirm.component';
// Application
import { MailingListService } from '../shared/mailing-list.service';

@Component({
  selector: 'app-mailing-list-list',
  templateUrl: './mailing-list-list.component.html',
  providers: [MailingListService]
})
export class MailingListListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    actions: Object.assign({} , Actions, { edit: false, delete: false }),
    columns: {
      name: { title: 'Lista', filter: false }
    }
  });

  /**
   * Constructs an MailingListListComponent instance.
   * @param {DatePipe} datePipe                      DatePipe injected instance
   * @param {LocalTimePipe} localTimePipe
   *  LocalTimePipe injected instance
   * @param {MailingListService} mailingListService
   *  SuggestionService injected instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private mailingListService: MailingListService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.mailingListService.list()
      .then(mailingLists => this.source.load(mailingLists));
  }

  /**
   * onCreate Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate(['pages/mailing-lists/new']);
  }

  /**
   * onEdit Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/mailing-lists/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, {
      size: 'lg',
      container: 'nb-layout'
    });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que lo quieres borrar?';
    activeModal.result.then((result) => {
      if (result) {
        this.mailingListService.destroy(event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Get shop clusters via the service.
   *
   * @private
   */
  private load(): void {
    this.mailingListService.list()
      .then(mailingLists => this.source.load(mailingLists));
  }
}
