/**
 * Kazam Admin
 * @name MailingLists Component
 * @file src/app/pages/mailing-lists/mailing-lists.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-mailing-lists',
  template: `<router-outlet></router-outlet>`
})
export class MailingListsComponent {}
