/**
 * Kazam Admin
 * @name MailingList model
 * @file src/app/pages/mailing-lists/mailing-list-list/mailing-lists.model.ts
 * @author Joel Cano
 */
export class MailingList {
  id: number;
  name: string;
  identity: number;
  mailchimp_id: string;
}
