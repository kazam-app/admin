/**
 * Kazam Admin
 * @name MailingList Service
 * @file src/app/pages/mailing-lists/mailing-list-list/mailing-list.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { MailingList } from './mailing-list.model';
import { DataService } from '../../../@core/data/data.service';

@Injectable()
export class MailingListService extends DataService {
  /**
   * Constructs an MailingListService instance.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/mailing_lists');
  }

  /**
   * Get MailingList from
   * @return {Promise<MailingList[]>} Suggestion data
   */
  list(): Promise<MailingList[]> {
    return this.get<MailingList[]>();
  }

  /**
   * Get a single MailingList.
   * @param {number} id               MailingList id
   * @return {Promise<MailingList>}   MailingList promise
   */
  show(id: number): Promise<MailingList> {
    return this.get<MailingList>(`/${id}`);
  }

  /**
   * Create a MailingList.
   * @param {MailingList} mailingList   MailingList data
   * @return {Promise<MailingList>}     MailingList instance promise
   */
  create(mailingList: MailingList): Promise<MailingList> {
    return this.post<MailingList>({ mailing_list: mailingList });
  }

  /**
   * Update a MailingList by id.
   * @param {MailingList} shopCluster   MailingList data
   * @return {Promise<MailingList>}     MailingList instance promise
   */
  update(mailingList: MailingList): Promise<MailingList> {
    return this.put<MailingList>(`/${mailingList.id}`, {
      mailing_list: mailingList
    });
  }

  /**
   * Remove a MailingList.
   * @return {Promise<MailingList>}  MailingList instance promise
   */
  destroy(id: number): Promise<MailingList> {
    return this.delete<MailingList>(`/${id}`);
  }
}
