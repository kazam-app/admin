/**
 * Kazam Admin
 * @name MailingList Component
 * @file src/app/pages/mailing-lists/mailing-list/mailing-list.component.ts
 * @author Joel Cano
 */

// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { MailingList } from '../shared/mailing-list.model';
import { FormComponent } from '../../shared/form/form.component';
import { MailingListService } from '../shared/mailing-list.service';

@Component({
  selector: 'app-mailing-list',
  templateUrl: './mailing-list.component.html',
  providers: [MailingListService]
})
export class MailingListComponent extends FormComponent<MailingList> implements OnInit {
  /**
   * Constructs an MailingListComponent instance.
   * @param {Router} router                           Router instance
   * @param {ActivatedRoute} route                    ActivatedRoute instance
   * @param {MailingListService} mailingListService   MailingListService
   *  instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private mailingListService: MailingListService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.mailingListService.update(this.element);
    } else {
      submision = this.mailingListService.create(this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * Set category via service.
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.mailingListService.show(id)
      .then(element => this.element = element)
      .then(() => this.loaded = true);
  }

  /**
   * New element
   *
   * @protected
   */
  protected newElement(): void {
    this.element = new MailingList();
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate(['pages/mailing-lists/list']);
  }
}
