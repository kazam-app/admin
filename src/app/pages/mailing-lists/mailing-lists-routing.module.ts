/**
 * Kazam Admin
 * @name MailingList Routing Module
 * @file src/app/pages/mailing-list/mailing-list-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { MailingListsComponent } from './mailing-lists.component';
import { MailingListComponent } from './mailing-list/mailing-list.component';
import {
  MailingListListComponent
} from './mailing-list-list/mailing-list-list.component';

const routes: Routes = [{
  path: '',
  component: MailingListsComponent,
  children: [{
    path: 'list',
    component: MailingListListComponent
  }, {
    path: 'new',
    component: MailingListComponent
  }, {
    path: ':id',
    component: MailingListComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MailingListRoutingModule {}

export const routedComponents = [
  MailingListComponent,
  MailingListsComponent,
  MailingListListComponent
];
