/**
 * Kazam Admin
 * @name PunchCard model
 * @file src/app/pages/punch-cards/shared/punch-card.model.ts
 * @author Joel Cano
 */
export const maxPunches = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
export const dateFormat = 'DD-MM-YYYY';
export const Identities = [
  'Sin publicar', // => 0
  'Publicada', // => 1
  'Expirada' // => 2
];

export class PunchCard {
  id: number;
  identity: number;
  name: string;
  merchant: string;
  merchant_id: number;
  punch_limit: number;
  expires_at: Date;
  prize: string;
  rules: string;
  terms: string;
  validation_limit: number;
  validation_limit_type: number;
}
