/**
 * Kazam Admin
 * @name PunchCardList Component
 * @file src/app/pages/punch-card-list/punch-card-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalTimePipe, DateFormatPipe } from 'angular2-moment';
import { IdentityPipe } from '../../../@theme/pipes/identity.pipe';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import {
  KzmConfirmComponent
} from '../../../@theme/components/modal/confirm.component';
// Application
import { PunchCardService } from '../shared/punch-card.service';
import { dateFormat, Identities } from '../shared/punch-card.model';

@Component({
  selector: 'app-punch-card-list',
  templateUrl: './punch-card-list.component.html',
  providers: [LocalTimePipe, DateFormatPipe, IdentityPipe]
})
export class PunchCardListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    columns: {
      merchant: { title: 'Marca', filter: false },
      name: { title: 'Nombre', filter: false },
      punch_limit: { title: 'Sellos', filter: false },
      identity: {
        title: 'Estado',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.identityPipe.transform(value, Identities);
        }
      },
      expires_at: { title: 'Expira (UTC)', filter: false },
      expires_at_format: {
        title: 'Expira (Local)',
        filter: false,
        valuePrepareFunction: (cell, row) => {
          const local = this.localTimePipe.transform(row.expires_at);
          return this.dateFormatPipe.transform(local, dateFormat);
        }
      }
    }
  });

  /**
   * Constructs an PunchCardListComponent instance.
   * @param {Router} router                       Router instance
   * @param {NgbModal} modalService               NgbModal instance
   * @param {IdentityPipe} identityPipe           IdentityPipe instance
   * @param {LocalTimePipe} localTimePipe         LocalTimePipe instance
   * @param {DateFormatPipe} dateFormatPipe       DateFormatPipe instance
   * @param {PunchCardService} punchCardService   PunchCardService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private identityPipe: IdentityPipe,
    private localTimePipe: LocalTimePipe,
    private dateFormatPipe: DateFormatPipe,
    private punchCardService: PunchCardService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.load();
  }

  /**
   * onCreate Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate(['pages/punch-cards/new']);
  }

  /**
   * onEdit Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/punch-cards/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const modal = this.modalService.open(KzmConfirmComponent, {
      size: 'lg',
      container: 'nb-layout'
    });
    modal.componentInstance.modalHeader = '¿Estas seguro que lo quieres borrar?';
    modal.result.then((result) => {
      if (result) {
        this.punchCardService.destroy(event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Retrive a data array from the server and load it as the table source.
   *
   * @private
   */
  private load(): void {
    this.punchCardService.list().then(punchCard => this.source.load(punchCard));
  }
}
