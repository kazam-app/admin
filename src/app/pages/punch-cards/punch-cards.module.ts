/**
 * Kazam Admin
 * @name PunchCard Module
 * @file src/app/pages/punch-punchs/punch-cards.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { PunchCardsRoutingModule, routedComponents } from './punch-cards-routing.module';
import { PunchCardService } from './shared/punch-card.service';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    ThemeModule,
    // Component
    PunchCardsRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents],
  providers: [PunchCardService],
  entryComponents: [KzmConfirmComponent]
})
export class PunchCardsModule {}
