/**
 * Kazam Web
 * @name PunchCard Component
 * @file src/app/pages/beacons/beacon/beacon.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Theme
import { LocalTimePipe } from 'angular2-moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  KzmConfirmComponent
} from '../../../@theme/components/modal/confirm.component';
// Application
import { PunchCardService } from '../shared/punch-card.service';
import { Merchant } from '../../../@core/models/merchant.model';
import { FormComponent } from '../../shared/form/form.component';
import { MerchantService } from '../../../@core/data/merchant.service';
import { PunchCard, maxPunches, Identities } from '../shared/punch-card.model';

@Component({
  selector: 'app-punch-card',
  templateUrl: './punch-card.component.html',
  providers: [LocalTimePipe]
})
export class PunchCardComponent extends FormComponent<PunchCard> implements OnInit {
  unpublished = true;
  punches = maxPunches;
  merchants: Merchant[];
  identities = Identities;

  /**
   * Constructs a PunchCardComponent instance.
   * @param {Router} router                       Router injected instance
   * @param {NgbModal} modalService               NgbModal injected instance
   * @param {ActivatedRoute} route                ActivatedRoute injected instance
   * @param {MerchantService} merchantService     MerchantService injected instance
   * @param {PunchCardService} punchCardService   PunchCardService injected instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    private modalService: NgbModal,
    protected route: ActivatedRoute,
    private localTimePipe: LocalTimePipe,
    private merchantService: MerchantService,
    private punchCardService: PunchCardService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    // expiresAt to localtime
    const expiresAt = this.localTimePipe.transform(
      this.element.expires_at
    ).toDate();
    this.element.expires_at = expiresAt;
    if (this.element.id) {
      submision = this.punchCardService.update(this.element);
    } else {
      submision = this.punchCardService.create(this.element);
    }
    submision.then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * PunchCard publish event
   */
  publish(): void {
    const modal = this.modalService.open(KzmConfirmComponent, {
      size: 'lg',
      container: 'nb-layout'
    });
    modal.componentInstance.modalHeader = '¿Estas seguro que la quieres publicar?';
    modal.result.then((accepted) => {
      if (accepted) {
        this.punchCardService.publish(this.element.id)
          .then((result) => this.published(result))
          .catch((err) => this.error(err));
      }
    });
  }

  /**
   * get PunchCard via id
   * @param {number} id   PunchCard id
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.punchCardService.show(id)
      .then(element => this.element = element)
      .then(() => {
        this.unpublished = this.element.identity === 0;
        this.loaded = true;
      });
  }

  /**
   * New element
   *
   * @protected
   */
  protected newElement(): void {
    this.merchantService.list().then(merchants => this.merchants = merchants);
    this.element = new PunchCard();
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate(['pages/punch-cards/list']);
  }

  /**
   * Publish success event
   *
   * @private
   */
  private published(result: any): void {
    this.success();
  }
}
