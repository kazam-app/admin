/**
 * Kazam Admin
 * @name Shared Module
 * @file src/app/pages/shared/shared.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Theme
import { ThemeModule } from '../../@theme/theme.module';
// Application
import { DataCardComponent } from './data-card/data-card.component';

const components = [DataCardComponent];

@NgModule({
  imports: [
    // Angular
    CommonModule,
    // Theme
    ThemeModule
  ],
  exports: [...components],
  declarations: [...components]
 })
 export class SharedModule {}
