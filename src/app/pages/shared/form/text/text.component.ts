/**
 * Kazam Admin
 * @name Text Component
 * @file src/app/pages/form/text/text.component.ts
 * @author Joel Cano
 */
// Angular
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, Input, HostBinding, forwardRef } from '@angular/core';

@Component({
  selector: 'kzm-text-input',
  templateUrl: './text.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => KzmTextComponent),
      multi: true
    }
  ]
})
export class KzmTextComponent implements ControlValueAccessor {

  value: string | number;
  _label: string;

  @Input() id: string;
  @Input() name: string;

  @Input('label')
  get label(): string {
    let result = this._label;
    if (this.required) {
      result += '*';
    }
    return result;
  }

  @Input() required = false;

  /**
   * Sets the component value and calls the change callback.
   * @param {string | number} val  Incoming value
   */
  writeValue(val: string | number): void {
    this.value = val == null ? '' : val;
    this.onChange(this.value);
  }

  // Available callbacks
  onChange = (val: string | number): void => {};
  onTouched = (): void => {};

  // Register callbacks
  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  registerOnChange(fn: (val: string | number) => void): void {
    this.onChange = fn;
  }
}
