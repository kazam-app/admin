/*
 * Kazam Admin
 * @name Form Module
 * @file src/app/pages/form/form.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Application
import { KzmTextComponent } from './text/text.component';

const baseModules = [
  FormsModule
];

const components = [
  KzmTextComponent
];

@NgModule({
  imports: [...baseModules],
  declarations: [...components],
  exports: [...baseModules, ...components]
})
export class FormModule {}
