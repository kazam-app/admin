/**
 * Kazam Admin
 * @name Form Component
 * @file src/app/pages/shared/form.component.ts
 * @author Joel Cano
 */
import { Router, ActivatedRoute } from '@angular/router';

export class FormComponent<T> {
  element: T;
  action: string;
  loaded = false;
  errors: string[] = [];
  constructor(
    protected router: Router,
    protected route: ActivatedRoute) {}
  /**
   * Get id param from route initialize the view.
   *
   * @protected
   */
  protected viewOnInit(): void {
    this.route.params.map(p => p.id).subscribe(this.setViewfor.bind(this));
  }
  /**
   * Parse incomming errors.
   *
   * @protected
   */
  protected error(errors: any): void {
    this.errors = Object.keys(errors).map(key => `${key} - ${errors[key]}`);
  }
  /**
   * Abstract function to set the element.
   * @param {number} id Generic T instance id
   *
   * @protected
   */
  protected getElement(id: number): void {}
  /**
   * Abstract function to create a new element instance.
   *
   * @protected
   */
  protected newElement(): void {}
  /**
   * Set view for id
   *
   * @private
   */
  private setViewfor(id: any): void {
    const val = parseInt(id, 10);
    if (isNaN(val)) {
      this.newElement();
      this.loaded = true;
      this.action = 'Crear';
    } else {
      this.getElement(val);
      this.action = 'Editar';
    }
  }
}
