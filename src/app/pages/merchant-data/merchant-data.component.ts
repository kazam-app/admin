/**
 * Kazam Admin
 * @name MerchantData Component
 * @file src/app/pages/merchant-data/merchant-data.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-merchant-data',
  template: `<router-outlet></router-outlet>`
})
export class MerchantDataComponent {}
