/**
 * Kazam Admin
 * @name Breakdown Service
 * @file src/app/pages/merchant-data/breakdown/breakdown.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { Breakdown } from './breakdown.model';
import { DataService } from '../../../@core/data/data.service';

@Injectable()
export class BreakdownService extends DataService {
  /**
   * Constructs an BreakdownService instance.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/merchants');
  }
  /**
   * Get all Breakdown entries for a certain token.
   * @param {number} merchantId   Merchant id
   * @param {Moment} startsAt     Moment date instance
   * @param {Moment} endsAt       Moment date instance
   * @return {Promise<Breakdown>} Breakdown promise
   */
  list(merchantId: number, startsAt: any, endsAt: any): Promise<Breakdown> {
    return this.get<Breakdown>(`/${merchantId}/breakdown`, {
      starts_at: startsAt.toISOString(),
      ends_at: endsAt.toISOString()
    });
  }
}
