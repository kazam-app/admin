/**
 * Kazam Admin
 * @name Breakdown Model
 * @file src/app/pages/merchant-data/breakdown/breakdown.model.ts
 * @author Joel Cano
 */
 export class Breakdown {
   shops: any;
   intervals: number;
 }
