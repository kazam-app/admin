/**
 * Kazam Admin
 * @name MerchantData Module
 * @file src/app/pages/merchant-data/merchant-data.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ThemeModule } from '../../@theme/theme.module';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { SharedModule } from '../shared/shared.module';
import { D3BarComponent } from './analytics/d3-bar/d3-bar.component';
import { DataCardComponent } from '../shared/data-card/data-card.component';
import { MerchantDataRoutingModule, routedComponents } from './merchant-data-routing.module';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    ThemeModule,
    // Component
    SharedModule,
    NgxChartsModule,
    Ng2SmartTableModule,
    MerchantDataRoutingModule
  ],
  declarations: [
    ...routedComponents,
    D3BarComponent
  ],
  providers: [],
  entryComponents: [KzmConfirmComponent, DataCardComponent]
 })
 export class MerchantDataModule {}
