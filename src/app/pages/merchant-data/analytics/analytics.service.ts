/**
 * Kazam Admin
 * @name Analytics Service
 * @file src/app/pages/merchant-data/analytics/analytics.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { Analytics } from './analytics.model';
import { DataService } from '../../../@core/data/data.service';

@Injectable()
export class AnalyticsService extends DataService {
  /**
   * Constructs an AnalyticsService instance.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/merchants');
  }
  /**
   * Get analytics data for a certain token.
   * @param {number} merchantId   Merchant id
   * @return {Promise<Analytics>} Analytics data
   */
  data(merchantId: number): Promise<Analytics> {
    return this.get<Analytics>(`/${merchantId}/analytics`);
  }
}
