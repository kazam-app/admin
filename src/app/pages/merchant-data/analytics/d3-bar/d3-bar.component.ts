/**
 * Kazam Admin
 * @name D3 Bar Component
 * @file src/app/pages/merchant-data/analytics/d3-bar/d3-bar.component.ts
 * @author Joel Cano
 */
import { Component, OnDestroy, Input } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'kzm-d3-bar',
  template: `
    <ngx-charts-bar-vertical
      [scheme]="colorScheme"
      [results]="results"
      [xAxis]="showXAxis"
      [yAxis]="showYAxis"
      [legend]="showLegend"
      [xAxisLabel]="xAxisLabel"
      [yAxisLabel]="yAxisLabel">
    </ngx-charts-bar-vertical>
  `,
})
export class D3BarComponent implements OnDestroy {
  @Input() results: any[];
  @Input() xAxisLabel: string;
  @Input() yAxisLabel: string;
  showLegend = false;
  showXAxis = true;
  showYAxis = true;
  colorScheme: any;
  themeSubscription: any;

  /**
   * Constructs an D3BarComponent instance.
   * @param {NbThemeService} theme     NbThemeService instance
   *
   * @constructor
   */
  constructor(private theme: NbThemeService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
  }

  /**
   * ngOnDestroy lifecycle callback
   */
  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
