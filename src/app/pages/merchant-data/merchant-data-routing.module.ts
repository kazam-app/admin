/**
 * Kazam Admin
 * @name MerchantData Routing Module
 * @file src/app/pages/merchant-data/merchant-data-routing.module.ts
 * @author Joel Cano
 */

// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { UserComponent } from './user/user.component';
import { HistoryComponent } from './history/history.component';
import { MerchantDataComponent } from './merchant-data.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { BreakdownComponent } from './breakdown/breakdown.component';
import { MerchantListComponent } from './merchant-list/merchant-list.component';

const routes: Routes = [{
  path: '',
  component: MerchantDataComponent,
  children: [{
    path: 'list',
    component: MerchantListComponent
  }, {
    path: ':merchantId/history',
    component: HistoryComponent
  }, {
    path: ':merchantId/analytics',
    component: AnalyticsComponent
  }, {
    path: ':merchantId/breakdown',
    component: BreakdownComponent
  }, {
    path: ':merchantId/users/:id',
    component: UserComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantDataRoutingModule {}

export const routedComponents = [
  UserComponent,
  HistoryComponent,
  AnalyticsComponent,
  BreakdownComponent,
  MerchantDataComponent,
  MerchantListComponent
];
