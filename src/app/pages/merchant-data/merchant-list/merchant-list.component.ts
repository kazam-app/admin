/**
 * Kazam Admin
 * @name MerchantList Component
 * @file src/app/pages/merchant-data/merchant-list/merchant-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
// Application
import { MerchantService } from '../../../@core/data/merchant.service';
import { ActionComponent } from '../../shared/action/action.component';

@Component({
  selector: 'app-data-merchant-list',
  templateUrl: './merchant-list.component.html'
})
export class MerchantListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = {
    mode: 'external',
    actions: { add: false, edit: false, delete: false },
    columns: {
      name: { title: 'Nombre', filter: false },
      category: { title: 'Categoría', filter: false },
      organization: { title: 'Organización', filter: false },
      history: {
        sort: false,
        filter: false,
        type: 'custom',
        title: 'Historial',
        renderComponent: ActionComponent,
        onComponentInitFunction (instance) {
          instance.renderValue = 'list';
          instance.action.subscribe(row => {
            instance.router.navigate([`pages/merchant-data/${row.id}/history`]);
          });
        }
      },
      analytics: {
        sort: false,
        filter: false,
        type: 'custom',
        title: 'Analytics',
        renderComponent: ActionComponent,
        onComponentInitFunction(instance) {
          instance.renderValue = 'bar-chart';
          instance.action.subscribe(row => {
            instance.router.navigate([`pages/merchant-data/${row.id}/analytics`]);
          });
        }
      },
      breakdown: {
        sort: false,
        filter: false,
        type: 'custom',
        title: 'Breakdown',
        renderComponent: ActionComponent,
        onComponentInitFunction: (instance) => {
          instance.renderValue = 'layout-sidebar-left';
          instance.action.subscribe(row => {
            this.merchantService.setCreatedAt(row);
            instance.router.navigate([`pages/merchant-data/${row.id}/breakdown`]);
          });
        }
      }
    }
  };

  /**
   * Constructs an MerchantListComponent instance.
   * @param {Router} router                     Router instance
   * @param {MerchantService} merchantService   MerchantService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private merchantService: MerchantService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.merchantService.list().then(merchants => this.source.load(merchants));
  }
}
