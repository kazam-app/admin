/**
 * Kazam Admin
 * @name Entry model
 * @file src/app/pages/merchants/history/entry.model.ts
 * @author Joel Cano
 */
export const Identities = ['Sello', 'Canje'];

export class Entry {
  id: number;
  user: string;
  name: string;
  last_names: string;
  identity: number;
  email: string;
  created_at: string;
  shop: string;
}
