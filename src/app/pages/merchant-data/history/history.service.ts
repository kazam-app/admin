/**
 * Kazam Admin
 * @name History Service
 * @file src/app/pages/merchant-data/history/history.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { Entry } from './entry.model';
import { DataService } from '../../../@core/data/data.service';

@Injectable()
export class HistoryService extends DataService {
  /**
   * Constructs an HistoryService instance.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/merchants');
  }
  /**
   * Get all History entries for a certain token.
   * @return {Promise<Entry[]>} Shops array promise
   */
  list(merchantId: number): Promise<Entry[]> {
    return this.get<any>(`/${merchantId}/history`);
  }
}
