/**
 * Kazam Admin
 * @name History Component
 * @file src/app/pages/merchant-data/history/history.component.ts
 * @author Joel Cano
 */
// Angular
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalTimePipe } from 'angular2-moment';
import { LocalDataSource } from 'ng2-smart-table';
import { IdentityPipe } from '../../../@theme/pipes/identity.pipe';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
// Application
import { Identities } from './entry.model';
import { HistoryService } from './history.service';
import { ActionComponent } from '../../shared/action/action.component';

@Component({
  selector: 'app-data-history',
  templateUrl: './history.component.html',
  providers: [IdentityPipe, DatePipe, LocalTimePipe, HistoryService]
})
export class HistoryComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  merchantId: number;
  settings = Object.assign({} , TableSettings, {
    actions: false,
    pager: { perPage: 20 },
    columns: {
      id: { title: 'Evento', filter: false },
      user: { title: 'Usuario', filter: false },
      name: { title: 'Nombre', filter: false },
      email: { title: 'Correo', filter: false },
      created_at: {
        title: 'Fecha',
        filter: false,
        valuePrepareFunction: (value) => {
          const local = this.localTimePipe.transform(value);
          return this.datePipe.transform(local, 'medium');
        }
      },
      identity: {
        title: 'Acción',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.identityPipe.transform(value, Identities);
        }
      },
      shop: { title: 'Tienda', filter: false },
      detail: {
        sort: false,
        filter: false,
        type: 'custom',
        title: 'Detalle',
        renderComponent: ActionComponent,
        onComponentInitFunction: (instance) => {
          instance.renderValue = 'person';
          instance.action.subscribe(row => {
            instance.router.navigate(
              [`pages/merchant-data/${this.merchantId}/users/${row.user}`]
            );
          });
        }
      }
    }
  });

  /**
   * Constructs an HistoryComponent instance.
   * @param {DatePipe} datePipe               DatePipe injected instance
   * @param {ActivatedRoute} route            ActivatedRoute injected instance
   * @param {IdentityPipe} identityPipe       IdentityPipe injected instance
   * @param {LocalTimePipe} localTimePipe     LocalTimePipe injected instance
   * @param {HistoryService} historyService   HistoryService injected instance
   *
   * @constructor
   */
  constructor(
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private identityPipe: IdentityPipe,
    private localTimePipe: LocalTimePipe,
    private historyService: HistoryService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.route.params.map(p => p.merchantId).subscribe(id => {
      this.merchantId = id;
      this.getHistory();
    });
  }

  /**
   * Get entries via the service.
   *
   * @private
   */
  private getHistory(): void {
    this.historyService.list(this.merchantId)
      .then(history => this.source.load(history));
  }
}
