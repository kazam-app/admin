/**
 * Kazam Admin
 * @name User Component
 * @file src/app/pages/users/user/user.component.ts
 * @author Joel Cano
 */
// Angular
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
// Theme
import { NbThemeService } from '@nebular/theme';
import { LocalTimePipe } from 'angular2-moment';
import { LocalDataSource } from 'ng2-smart-table';
import { ActivePipe } from '../../../@theme/pipes/active.pipe';
import { IdentityPipe } from '../../../@theme/pipes/identity.pipe';
// Application
import { User, Genders } from '../shared/user.model';
import { UserService } from '../shared/user.service';

const Identities = ['Sello', 'Canje'];

@Component({
  selector: 'app-user-detail',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [DatePipe, LocalTimePipe, ActivePipe, IdentityPipe]
})
export class UserComponent implements OnInit, OnDestroy {
  user: User;
  userId: number;
  genders = Genders;
  topSettings = {
    mode: 'external',
    actions: false,
    columns: {
      name: { title: 'Nombre', filter: false },
      total: { title: 'Interaciones', filter: false }
    }
  };

  // Merchants
  merchantSource: LocalDataSource = new LocalDataSource();
  // Shops
  shopSource: LocalDataSource = new LocalDataSource();

  historySettings = {
    mode: 'external',
    actions: false,
    columns: {
      id: { title: 'Evento', filter: false },
      identity: {
        title: 'Acción',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.identityPipe.transform(value, Identities);
        }
      },
      created_at: {
        title: 'Fecha',
        filter: false,
        valuePrepareFunction: (value) => {
          const local = this.localTimePipe.transform(value);
          return this.datePipe.transform(local, 'medium');
        }
      },
      merchant: { title: 'Marca', filter: false },
      shop: { title: 'Tienda', filter: false }
    }
  };

  // History
  historySource: LocalDataSource = new LocalDataSource();

  // Categories
  colorScheme: any;
  showLegend = true;
  showLabels = true;
  categories: any[] = [];
  themeSubscription: any;
  view: any[] = [700, 400];

  // Averages
  punchAverages: any;
  redeemAverages: any;

  /**
   * Constructs an UserComponent instance.
   * @param {DatePipe} route                DatePipe instance
   * @param {ActivatedRoute} route          ActivatedRoute instance
   * @param {UserService} userService       UserService instance
   * @param {LocalTimePipe} localTimePipe   LocalTimePipe instance
   *
   * @constructor
   */
  constructor(
    private datePipe: DatePipe,
    private theme: NbThemeService,
    private route: ActivatedRoute,
    private userService: UserService,
    private identityPipe: IdentityPipe,
    private localTimePipe: LocalTimePipe) {
      this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
        const colors: any = config.variables;
        this.colorScheme = {
          domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight]
        };
      });
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.route.params.map(p => p.id).subscribe(id => {
      this.userId = id;
      this.getUser();
    });
  }

  /**
   * ngOnDestroy lifecycle callback
   */
  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

  /**
   * Get user details via the service.
   *
   */
  setTooltipText(element): string {
    return `${element.data.name}: ${element.value}%`;
  }

  /**
   * Get user details via the service.
   *
   * @private
   */
  private getUser(): void {
    this.userService.show(this.userId)
      .then(user => this.user = user)
      .then(() => {
        this.merchantSource.load(this.user.top_merchants);
        this.shopSource.load(this.user.top_shops);
        this.historySource.load(this.user.history);
        this.categories = this.user.categories;
        this.punchAverages = this.user.averages['0'];
        if (this.punchAverages) {
          this.punchAverages = this.punchAverages[0];
        }
        this.redeemAverages = this.user.averages['1'];
        if (this.redeemAverages) {
          this.redeemAverages = this.redeemAverages[0];
        }
      });
  }
}
