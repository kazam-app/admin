/**
 * Kazam Admin
 * @name Users Routing Module
 * @file src/app/pages/users/users-routing.module.ts
 * @author Joel Cano
 */

// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { UsersComponent } from './users.component';
import { UserComponent } from './user/user.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [{
    path: 'list',
    component: UserListComponent
  }, {
    path: ':id',
    component: UserComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}

export const routedComponents = [
  UserComponent,
  UsersComponent,
  UserListComponent
];
