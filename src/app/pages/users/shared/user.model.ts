/**
 * Kazam Admin
 * @name User model
 * @file src/app/pages/users/user.model.ts
 * @author Joel Cano
 */
export const Genders = ['Femenino', 'Masculino'];

export class User {
  id: number;
  name: string;
  last_names: string;
  email: string;
  is_verified: boolean;
  sign_up_type: string;
  gender: number;
  birthdate: Date;
  created_at: Date;
  top_merchants: any;
  top_shops: any;
  history: any;
  categories: any;
  averages: any;
}
