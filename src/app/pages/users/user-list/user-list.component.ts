/**
 * Kazam Admin
 * @name UserList Component
 * @file src/app/pages/user-list/user-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { ActivePipe } from '../../../@theme/pipes/active.pipe';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
// Application
import { UserService } from '../shared/user.service';
import { ActionComponent } from '../../shared/action/action.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  providers: [ActivePipe]
})
export class UserListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings =  Object.assign({} , TableSettings, {
    actions: false,
    columns: {
      name: { title: 'Nombre', filter: false },
      last_names: { title: 'Apellido', filter: false },
      email: { title: 'Correo', filter: false },
      is_verified: {
        title: 'Verificado',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.activePipe.transform(value, 'Verificado', 'Sin Verificar');
        }
      },
      detail: {
        title: 'Detalle',
        filter: false,
        type: 'custom',
        renderComponent: ActionComponent,
        onComponentInitFunction (instance) {
          instance.renderValue = 'person';
          instance.action.subscribe(row => {
            instance.router.navigate([`pages/users/${row.id}`]);
          });
        }
      }
    }
  });

  /**
   * Constructs an UserListComponent instance.
   * @param {ActivePipe} activePipe       ActivePipe instance
   * @param {UserService} userService     UserService injected instance
   *
   * @constructor
   */
  constructor(
    private activePipe: ActivePipe,
    private userService: UserService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.userService.list().then(users => this.source.load(users));
  }
}
