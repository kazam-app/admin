/**
 * Kazam Admin
 * @name Users Module
 * @file src/app/pages/users/users.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ThemeModule } from '../../@theme/theme.module';
// Application
import { UserService } from './shared/user.service';
import { SharedModule } from '../shared/shared.module';
import { DataCardComponent } from '../shared/data-card/data-card.component';
import { UsersRoutingModule, routedComponents } from './users-routing.module';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    // Theme
    ThemeModule,
    // Component
    SharedModule,
    NgxChartsModule,
    Ng2SmartTableModule,
    UsersRoutingModule
  ],
  declarations: [
    ...routedComponents
  ],
  providers: [UserService],
  entryComponents: [DataCardComponent]
 })
 export class UsersModule {}
