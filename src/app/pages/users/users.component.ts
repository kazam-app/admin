/**
 * Kazam Admin
 * @name Users Component
 * @file src/app/pages/users/users.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-users',
  template: `<router-outlet></router-outlet>`
})
export class UsersComponent {}
