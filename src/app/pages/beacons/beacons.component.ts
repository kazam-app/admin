/**
 * Kazam Admin
 * @name Beacons Component
 * @file src/app/pages/beacons/beacons.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-beacons',
  template: `<router-outlet></router-outlet>`
})
export class BeaconsComponent {}
