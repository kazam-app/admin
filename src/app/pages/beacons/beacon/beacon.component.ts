/**
 * Kazam Web
 * @name Beacon Component
 * @file src/app/pages/beacons/beacon/beacon.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { Beacon } from '../shared/beacon.model';
import { BeaconService } from '../shared/beacon.service';
import { Shop } from '../../../@core/models/shop.model';
import { ShopService } from '../../../@core/data/shop.service';
import { Merchant } from '../../../@core/models/merchant.model';
import { FormComponent } from '../../shared/form/form.component';
import { MerchantService } from '../../../@core/data/merchant.service';

@Component({
  selector: 'app-beacon',
  templateUrl: './beacon.component.html'
})
export class BeaconComponent extends FormComponent<Beacon> implements OnInit {
  merchants: Merchant[];
  shops: Shop[];

  /**
   * Constructs an BeaconComponent instance.
   * @param {Router} router                          Router injected instance
   * @param {ActivatedRoute} route                   ActivatedRoute injected instance
   * @param {ShopService} shopService                ShopService injected instance
   * @param {BeaconService} beaconService            BeaconService injected instance
   * @param {MerchantService} merchantService        MerchantService injected instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private shopService: ShopService,
    private beaconService: BeaconService,
    private merchantService: MerchantService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
    this.merchantService.list().then(merchants => this.merchants = merchants);
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.beaconService.update(this.element);
    } else {
      submision = this.beaconService.create(this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  loadShops(value: any): Promise<Shop[]> {
    this.shops = [];
    return this.shopService.list(parseInt(value, 10)).then(shops => this.shops = shops);
  }

  protected getElement(id: number): void {
    this.beaconService.show(id)
      .then(element => this.element = element)
      .then(() => {
        return this.loadShops(this.element.merchant_id);
      })
      .then(() => this.loaded = true);
  }

  protected newElement(): void {
    this.element = new Beacon();
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate(['pages/beacons']);
  }
}
