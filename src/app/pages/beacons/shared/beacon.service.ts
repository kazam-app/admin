/**
 * Kazam Web
 * @name Beacon Service
 * @file src/app/pages/beacons/shared/beacon.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from '../../../@core/data/data.service';
import { Beacon } from './beacon.model';

@Injectable()
export class BeaconService extends DataService {
  /**
   * Constructs an BeaconService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/beacons');
  }
  /**
   * Get all Beacons for a certain token.
   * @return {Promise<Beacon[]>} Beacons array promise
   */
  list(): Promise<Beacon[]> {
    return this.get<Beacon[]>();
  }
  /**
   * Get a Beacon by id.
   * @return {Promise<Beacon>} Beacon instance promise
   */
  show(id: number): Promise<Beacon> {
    return this.get<Beacon>(`/${id}`);
  }
  /**
   * Create a Beacon.
   * @return {Promise<Beacon>} Beacon instance promise
   */
  create(beacon: Beacon): Promise<Beacon> {
    return this.post<Beacon>({ beacon: beacon });
  }
  /**
   * Update a Beacon by id.
   * @return {Promise<Beacon>} Beacon instance promise
   */
  update(beacon: Beacon): Promise<Beacon> {
    return this.put<Beacon>(`/${beacon.id}`, { beacon: beacon });
  }
  /**
   * Remove a Beacon.
   * @return {Promise<Beacon>} Beacon instance promise
   */
  destroy(id: number): Promise<Beacon> {
    return this.delete<Beacon>(`/${id}`);
  }
}
