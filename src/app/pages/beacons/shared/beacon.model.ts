/**
 * Kazam Admin
 * @name Beacon model
 * @file src/app/pages/beacons/shared/beacon.model.ts
 * @author Joel Cano
 */
export class Beacon {
  id: number;
  is_active: boolean;
  major: number;
  minor: number;
  merchant: string;
  merchant_id: number;
  name: string;
  shop: string;
  shop_id: number;
  uid: string;
}
