/**
 * Kazam Admin
 * @name BeaconList Component
 * @file src/app/pages/beacons/beacon-list/beacon-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivePipe } from '../../../@theme/pipes/active.pipe';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { BeaconService } from '../shared/beacon.service';

@Component({
  selector: 'app-beacon-list',
  templateUrl: './beacon-list.component.html',
  providers: [ActivePipe]
})
export class BeaconListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    columns: {
      uid: { title: 'UUID', filter: false },
      major: { title: 'Major', filter: false },
      minor: { title: 'Minor', filter: false },
      name: { title: 'Clave', filter: false },
      merchant: { title: 'Marca', filter: false },
      shop: { title: 'Tienda', filter: false },
      is_active: {
        filter: false,
        title: 'Estado',
        valuePrepareFunction: (value) => {
          return this.activePipe.transform(value, 'Activo', 'Pausado');
        }
      }
    }
  });

  /**
   * Constructs an BeaconListComponent instance.
   * @param {Router} router                 Router instance
   * @param {NgbModal} modalService         NgbModal instance
   * @param {ActivePipe} activePipe         ActivePipe instance
   * @param {BeaconService} beaconService   BeaconService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private activePipe: ActivePipe,
    private beaconService: BeaconService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.load();
  }

  /**
   * onCreate Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate(['pages/beacons/new']);
  }

  /**
   * onEdit Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/beacons/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que lo quieres borrar?';
    activeModal.result.then((result) => {
      if (result) {
        this.beaconService.destroy(event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Retrive a data array from the server and load it as the table source.
   *
   * @private
   */
  private load(): void {
    this.beaconService.list().then(beacons => this.source.load(beacons));
  }
}
