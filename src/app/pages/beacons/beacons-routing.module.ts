/**
 * Kazam Admin
 * @name Beacons Routing Module
 * @file src/app/pages/beacons/beacons-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { BeaconsComponent } from './beacons.component';
import { BeaconComponent } from './beacon/beacon.component';
import { BeaconListComponent } from './beacon-list/beacon-list.component';

const routes: Routes = [{
  path: '',
  component: BeaconsComponent,
  children: [{
    path: 'list',
    component: BeaconListComponent
  }, {
    path: 'new',
    component: BeaconComponent
  }, {
    path: ':id',
    component: BeaconComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BeaconsRoutingModule {}

export const routedComponents = [
  BeaconsComponent,
  BeaconListComponent,
  BeaconComponent
];
