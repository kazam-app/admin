/**
 * Kazam Admin
 * @name Beacons Module
 * @file src/app/pages/beacons/beacons.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
// import { KzmTextComponent } from '../../pages/shared/form/text/text.component';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { BeaconsRoutingModule, routedComponents } from './beacons-routing.module';
import { BeaconService } from './shared/beacon.service';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    ThemeModule,
    // Component
    BeaconsRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents],
  providers: [BeaconService],
  entryComponents: [KzmConfirmComponent]
})
export class BeaconsModule {}
