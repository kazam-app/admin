/**
 * Kazam Admin
 * @name Pages Routing Module
 * @file src/app/pages/pages-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, {
    path: 'organizations',
    loadChildren: './organizations/organizations.module#OrganizationsModule'
  }, {
    path: 'categories',
    loadChildren: './categories/categories.module#CategoriesModule'
  }, {
    path: 'shop-clusters',
    loadChildren: './shop-clusters/shop-clusters.module#ShopClustersModule'
  }, {
    path: 'merchants',
    loadChildren: './merchants/merchants.module#MerchantsModule'
  }, {
    path: 'beacons',
    loadChildren: './beacons/beacons.module#BeaconsModule'
  }, {
    path: 'qrs',
    loadChildren: './qrs/qrs.module#QrsModule'
  }, {
    path: 'punch-cards',
    loadChildren: './punch-cards/punch-cards.module#PunchCardsModule'
  }, {
    path: 'suggestions',
    loadChildren: './suggestions/suggestions.module#SuggestionsModule'
  }, {
    path: 'merchant-data',
    loadChildren: './merchant-data/merchant-data.module#MerchantDataModule'
  }, {
    path: 'users',
    loadChildren: './users/users.module#UsersModule'
  }, {
    path: 'mailing-lists',
    loadChildren: './mailing-lists/mailing-lists.module#MailingListsModule'
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
