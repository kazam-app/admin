/**
 * Kazam Admin
 * @name Pages Component
 * @file src/app/pages/pages.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';
// Theme
import { NbMenuItem } from '@nebular/theme';

const items: NbMenuItem[] = [
  { title: 'Catálogos', group: true },
  {
    title: 'Organizaciones',
    icon: 'nb-grid-b-outline',
    children: [{
      title: 'Lista',
      link: '/pages/organizations/list'
    }, {
      title: 'Nueva',
      link: '/pages/organizations/new'
    }]
  },
  {
    title: 'Categorias',
    icon: 'nb-list',
    children: [{
      title: 'Lista',
      link: '/pages/categories/list'
    }, {
      title: 'Nueva',
      link: '/pages/categories/new'
    }]
  },
  {
    title: 'Clusters',
    icon: 'nb-location',
    children: [{
      title: 'Lista',
      link: '/pages/shop-clusters/list'
    }, {
      title: 'Nuevo',
      link: '/pages/shop-clusters/new'
    }]
  },
  { title: 'Comercios', group: true },
  {
    title: 'Marcas',
    icon: 'fa fa-building-o',
    children: [{
      title: 'Lista',
      link: '/pages/merchants/list'
    }, {
      title: 'Nueva',
      link: '/pages/merchants/new'
    }]
  },
  {
    title: 'Beacons',
    icon: 'nb-sunny-circled',
    children: [{
      title: 'Lista',
      link: '/pages/beacons/list'
    }, {
      title: 'Nuevo',
      link: '/pages/beacons/new'
    }]
  },
  {
    title: 'QRs',
    icon: 'fa fa-qrcode',
    children: [{
      title: 'Lista',
      link: '/pages/qrs/list'
    }, {
      title: 'Nuevo',
      link: '/pages/qrs/new'
    }]
  },
  {
    title: 'Tarjetas de Lealtad',
    icon: 'nb-layout-sidebar-left',
    children: [{
      title: 'Lista',
      link: '/pages/punch-cards/list'
    }, {
      title: 'Nueva',
      link: '/pages/punch-cards/new'
    }]
  },
  { title: 'Datos', group: true },
  {
    title: 'Sugerencias',
    icon: 'nb-list',
    children: [{
      title: 'Lista',
      link: '/pages/suggestions/list'
    }]
  },
  {
    title: 'Marcas',
    icon: 'fa fa-building-o',
    children: [{
      title: 'Lista',
      link: '/pages/merchant-data/list'
    }]
  },
  {
    title: 'Usuarios',
    icon: 'nb-person',
    children: [{
      title: 'Lista',
      link: '/pages/users/list'
    }]
  },
  {
    title: 'Listas de Correo',
    icon: 'nb-layout-sidebar-left',
    children: [{
      title: 'Lista',
      link: '/pages/mailing-lists/list'
    }]
  }
];

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html'
})
export class PagesComponent {
  menu: NbMenuItem[] = items;
}
