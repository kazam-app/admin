/**
 * Kazam Admin
 * @name CategoryList Component
 * @file src/app/pages/category-list/category-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { CategoryService } from '../../../@core/data/category.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html'
})
export class CategoryListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    columns: {
      name: { title: 'Nombre', filter: false }
    }
  });

  /**
   * Constructs an CategoryListComponent instance.
   * @param {Router} router                     Router instance
   * @param {NgbModal} modalService             NgbModal instance
   * @param {CategoryService} categoryService   CategoryService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private categoryService: CategoryService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.load();
  }

  /**
   * onCreate Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate(['pages/categories/new']);
  }

  /**
   * onEdit Event Callback
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/categories/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que lo quieres borrar?';
    activeModal.result.then((result) => {
      if (result) {
        this.categoryService.destroy(event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Retrive a data array from the server and load it as the table source.
   *
   * @private
   */
  private load(): void {
    this.categoryService.list().then(categories => this.source.load(categories));
  }
}
