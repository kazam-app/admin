/**
 * Kazam Admin
 * @name Categories Module
 * @file src/app/pages/categories/categories.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
// import { KzmTextComponent } from '../../pages/shared/form/text/text.component';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { CategoriesRoutingModule, routedComponents } from './categories-routing.module';
import { FormModule } from '../shared/form/form.module';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    ThemeModule,
    FormModule,
    // Component
    CategoriesRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents],
  entryComponents: [KzmConfirmComponent]
})
export class CategoriesModule {}
