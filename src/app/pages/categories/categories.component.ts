/**
 * Kazam Admin
 * @name Categories Component
 * @file src/app/pages/categories/categories.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-categories',
  template: `<router-outlet></router-outlet>`
})
export class CategoriesComponent {}
