/**
 * Kazam Admin
 * @name Categories Routing Module
 * @file src/app/pages/shops/shops-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { CategoriesComponent } from './categories.component';
import { CategoryComponent } from './category/category.component';
import { CategoryListComponent } from './category-list/category-list.component';

const routes: Routes = [{
  path: '',
  component: CategoriesComponent,
  children: [{
    path: 'list',
    component: CategoryListComponent
  }, {
    path: 'new',
    component: CategoryComponent
  }, {
    path: ':id',
    component: CategoryComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriesRoutingModule {}

export const routedComponents = [
  CategoriesComponent,
  CategoryListComponent,
  CategoryComponent
];
