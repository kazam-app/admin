/**
 * Kazam Admin
 * @name Category Component
 * @file src/app/pages/categories/category/category.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { Category } from '../../../@core/models/category.model';
import { FormComponent } from '../../shared/form/form.component';
import { ImageService } from '../../../@core/data/image.service';
import { CategoryService } from '../../../@core/data/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent extends FormComponent<Category> implements OnInit {
  imgLoaded = false;
  extensions: string;

  /**
   * Constructs an CategoryComponent instance.
   * @param {Router} router                     Angular Router instance
   * @param {ActivatedRoute} route              ActivatedRoute instance
   * @param {ImageService} imageService         ImageService instance
   * @param {CategoryService} categoryService   CategoryService instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private imageService: ImageService,
    private categoryService: CategoryService) {
      super(router, route);
      this.extensions = this.imageService.extensions;
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.categoryService.update(this.element);
    } else {
      submision = this.categoryService.create(this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * File changed event event
   * @param {any} event File event instance
   */
  fileChange(event) {
    const reader = this.imageService.fileChange(event);
    reader.onload = () => {
      this.element.image = reader.result;
      this.imgLoaded = true;
    };
  }

  /**
   * Set category via service.
   * @param {number} id Category id
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.categoryService.show(id)
      .then(element => this.element = element)
      .then(() => {
        this.loaded = true;
        this.imgLoaded = true;
      });
  }

  /**
   * New element
   *
   * @protected
   */
  protected newElement(): void {
    this.element = new Category();
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate(['pages/categories/list']);
  }
}
