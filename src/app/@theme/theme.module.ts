/**
 * Kazam Admin
 * @name Theme Module
 * @file src/app/@theme/theme.module.ts
 * @author Joel Cano
 */
// Angular imports
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Framework
import {
  NbActionsModule,
  NbCardModule,
  NbLayoutModule,
  NbMenuModule,
  NbRouteTabsetModule,
  NbSearchModule,
  NbSidebarModule,
  NbTabsetModule,
  NbThemeModule,
  NbUserModule,
  NbCheckboxModule,
} from '@nebular/theme';

// Angular
const baseModules = [CommonModule, FormsModule, ReactiveFormsModule, RouterModule];

// Nebular modules
const nbModules = [
  NbCardModule,
  NbLayoutModule,
  NbTabsetModule,
  NbRouteTabsetModule,
  NbMenuModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbCheckboxModule,
  NgbModule
];

// Theme components
import { KzmDashboardLayoutComponent } from './layouts';
import { KzmAuthComponent } from './components/auth/auth.component';
import { KzmLoginComponent } from './components/auth/login/login.component';
import { KzmLogoutComponent } from './components/auth/logout/logout.component';
import { KzmAuthBlockComponent } from './components/auth/auth-block/auth-block.component';
import {
  KzmFooterComponent,
  KzmHeaderComponent,
  KzmActionComponent,
  KzmConfirmComponent
} from './components';

const components = [
  KzmAuthComponent,
  KzmAuthBlockComponent,
  KzmLogoutComponent,
  KzmLoginComponent,
  KzmHeaderComponent,
  KzmFooterComponent,
  KzmActionComponent,
  KzmConfirmComponent,
  KzmDashboardLayoutComponent
];

// Theme pipes
import { RolePipe, MonthPipe, ActivePipe, IdentityPipe } from './pipes';
const pipes = [
  RolePipe,
  MonthPipe,
  ActivePipe,
  IdentityPipe
];

// Nebular theme providers
const nbProviders = [
  ...NbThemeModule.forRoot({ name: 'cosmic' }).providers,
  ...NbSidebarModule.forRoot().providers,
  ...NbMenuModule.forRoot().providers
];

@NgModule({
  imports: [...baseModules, ...nbModules],
  exports: [...baseModules, ...nbModules, ...components, ...pipes],
  declarations: [...components, ...pipes]
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeModule,
      providers: [...nbProviders]
    };
  }
}
