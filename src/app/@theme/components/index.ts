/**
 * Kazam Admin
 * @name Export Components
 * @file src/app/@theme/pipes/index.ts
 * @author Joel Cano
 */
export * from './header/header.component';
export * from './footer/footer.component';
export * from './modal/confirm.component';
export * from './action/action.component';
