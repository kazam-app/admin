/**
 * Kazam Admin
 * @name Logout Component
 * @file src/app/@theme/components/auth/logout/logout.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { NbAuthService, NbAuthResult } from '@nebular/auth';

@Component({
  selector: 'kzm-logout',
  template: `
    <div>Cerrando sesi&oacute;n.</div>
  `,
})
export class KzmLogoutComponent implements OnInit {
  redirectDelay = 100;

  /**
   * Constructs an KzmLogoutComponent instance.
   * @param {NbAuthService} tokenService         NbAuthService instance
   * @param {Router} router                      Angular Router instance
   *
   * @constructor
   */
  constructor(
    protected service: NbAuthService,
    protected router: Router) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.logout('email');
  }

  /**
   * Sends the delete request to the server and
   * @param {string} provider   Auth provider key
   */
  logout(provider: string): void {
    this.service.logout(provider).subscribe((result: NbAuthResult) => {
      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
    });
  }
}
