/**
 * Kazam Admin
 * @name Login Component
 * @file src/app/@theme/components/auth/login/login.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, Inject } from '@angular/core';
// Theme
import { getDeepFromObject } from '../helpers';
import { NB_AUTH_OPTIONS_TOKEN, NbAuthResult, NbAuthService } from '@nebular/auth';

@Component({
  selector: 'kzm-login',
  templateUrl: './login.component.html',
})
export class KzmLoginComponent {
  provider = '';
  redirectDelay = 0;
  showMessages: any = {};
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted = false;
  /**
   * Constructs an KzmLoginComponent instance.
   * @param {NbAuthService} service           NbAuthService instance
   * @param {NB_AUTH_OPTIONS_TOKEN} config    Default NbAuthService config
   * @param {Router} router                   Angular Router instance
   *
   * @constructor
   */
  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router) {
    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');
    this.provider = this.getConfigValue('forms.login.provider');
  }

  /**
   * Send login credentials to the API and display the result.
   */
  login(): void {
    this.errors = this.messages = [];
    this.submitted = true;

    this.service.authenticate(this.provider, this.user)
      .subscribe((result: NbAuthResult) => {
        this.submitted = false;
        if (result.isSuccess()) {
          this.messages = result.getMessages();
        } else {
          this.errors = result.getErrors();
        }
        const redirect = result.getRedirect();
        if (redirect) {
          setTimeout(() => {
            return this.router.navigateByUrl(redirect);
          }, this.redirectDelay);
        }
      });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}
