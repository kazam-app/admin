/**
 * Kazam Admin
 * @name AuthBlock Component
 * @file src/app/@theme/components/auth/auth-block/auth-block.component.ts
 * @author Joel Cano
 */
import { Component } from '@angular/core';

@Component({
  selector: 'kzm-auth-block',
  styleUrls: ['./auth-block.component.scss'],
  template: `
    <div class="auth-logo">
      <img src="assets/logo.png" />
    </div>
    <ng-content></ng-content>
  `,
})
export class KzmAuthBlockComponent {}
