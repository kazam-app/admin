/**
 * Kazam Admin
 * @name Footer Component
 * @file src/app/@theme/components/footer/footer.component.ts
 * @author Joel Cano
 */
import { Component } from '@angular/core';

@Component({
  selector: 'kzm-footer',
  styleUrls: ['./footer.component.scss'],
  templateUrl: './footer.component.html',
})
export class KzmFooterComponent {}
