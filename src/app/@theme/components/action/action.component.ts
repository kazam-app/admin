/**
 * Kazam Admin
 * @name Action Component
 * @file src/app/@theme/components/action/action.component.ts
 * @author Joel Cano
 */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'kzm-action',
  templateUrl: './action.component.html',
})
export class KzmActionComponent implements OnInit {
  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() actioned: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = 'Button';
  }

  onClick() {
    this.actioned.emit(this.rowData);
  }
}
