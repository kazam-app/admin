/**
 * Kazam Admin
 * @name Export Layouts
 * @file src/app/@theme/layouts/index.ts
 * @author Joel Cano
 */
export * from './dashboard/dashboard.layout';
