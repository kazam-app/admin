/**
 * Kazam Admin
 * @name Export Pipes
 * @file src/app/@theme/pipes/index.ts
 * @author Joel Cano
 */
export * from './role.pipe';
export * from './month.pipe';
export * from './active.pipe';
export * from './identity.pipe';
