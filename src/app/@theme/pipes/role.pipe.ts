/**
 * Kazam Admin
 * @name Role Pipe
 * @file src/app/@theme/pipes/role.pipe.ts
 * @author Joel Cano
 */
import { Pipe, PipeTransform } from '@angular/core';

const roles = [
  'Lectura',
  'Lectura y Escritura'
];

@Pipe({
  name: 'rolePipe'
})
export class RolePipe implements PipeTransform {

  transform(value: number): string {
    const role = roles[value];
    if (role) {
      return role;
    } else {
      return '-';
    }
  }
}
