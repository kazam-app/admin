/**
 * Kazam Admin
 * @name Routing Module
 * @file src/app/app-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
// Custom nebular auth components
import { KzmAuthComponent } from './@theme/components/auth/auth.component';
import { KzmLoginComponent } from './@theme/components/auth/login/login.component';
import { KzmLogoutComponent } from './@theme/components/auth/logout/logout.component';
import { KzmAuthBlockComponent } from './@theme/components/auth/auth-block/auth-block.component';
// Application
import { AuthGuard } from './@core/util/auth.guard';

const routes: Routes = [
  {
    path: 'pages',
    canActivate: [AuthGuard],
    loadChildren: 'app/pages/pages.module#PagesModule'
  },
  {
    path: 'auth',
    component: KzmAuthComponent,
    children: [
      { path: '', component: KzmLoginComponent },
      { path: 'login', component: KzmLoginComponent },
      { path: 'logout', component: KzmLogoutComponent }
   ]
 },
 { path: '', redirectTo: 'pages', pathMatch: 'full' },
 { path: '**', redirectTo: 'pages' }
];

const config: ExtraOptions = { useHash: true };

@NgModule({
 imports: [RouterModule.forRoot(routes, config)],
 exports: [RouterModule],
})
export class AppRoutingModule {}
